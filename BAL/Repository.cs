﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace BAL
{
    public class UserRepository : RepositoryBase<Tbl_Users>
    {
        public bool InsertUserdata(Tbl_Users users)
        {
            users.IsActive = true;
            users.CreatedOn = DateTime.Now;
            Insert(users);
            return true;
        }
        public bool Checkemailexist(string Email)
        {
            using (LogiqguruEntities context = new LogiqguruEntities())
            {
                Tbl_Users data = context.Tbl_Users.Where(x => x.UserName == Email).FirstOrDefault();
                if (data != null)
                    return true;
                else
                    return false;
            }
        }
        public bool Checkmobileexist(string Mobile)
        {
            using (LogiqguruEntities context = new LogiqguruEntities())
            {
                Tbl_Users data = context.Tbl_Users.Where(x => x.Mobile == Mobile).FirstOrDefault();
                if (data != null)
                    return true;
                else
                    return false;
            }
        }
        public Tbl_Users Getuserdatabasedoncredentials(string username, string password)
        {
            using (LogiqguruEntities context = new LogiqguruEntities())
            {
                return context.Tbl_Users.Where(x => x.UserName == username && x.Password == password).FirstOrDefault();
            }
        }

    }
    public class Classrepository : RepositoryBase<Tbl_Class_Info>
    {
        public List<Tbl_Class_Info> Getclasses()
        {
            using (LogiqguruEntities context = new LogiqguruEntities())
            {
                return context.Tbl_Class_Info.ToList();
            }
        }
    }
    public class Subusersrepository : RepositoryBase<Tbl_Subusers>
    {

        public bool Insersubusers(Tbl_Subusers subusers)
        {
            subusers.CreatedOn = DateTime.Now;
            Tbl_Subusers data = Insert(subusers);
            Worksheetrepository wrep = new Worksheetrepository();
            wrep.Insertfreeworksheets(data.SubUserId, data.ClassId);

            //Worksheetrepository worksheetrepository = new Worksheetrepository();
            //worksheetrepository.Insertfreeworksheets(data.SubUserId, data.ClassId);
            UpdateAwardsrepository urep = new UpdateAwardsrepository();
            UpdateAwardscurriculumrepository ucrep = new UpdateAwardscurriculumrepository();
            ucrep.Insertfirstrecord(data.SubUserId);
            urep.Insertfirstrecord(data.SubUserId);
            return true;
        }
        public bool Updatesub(Tbl_Subusers subusers)
        {
            Tbl_Subusers data = Get().Where(x => x.SubUserId == subusers.SubUserId).FirstOrDefault();
            data.ChildImage = subusers.ChildImage;
            data.City = subusers.City;
            data.ClassId = subusers.ClassId;
            data.FirstName = subusers.FirstName;
            data.LastName = subusers.LastName;
            data.School = subusers.School;
            Update(data);
            return true;
        }
        public UserswithSubs Getfulldataofuser(long subuserid)
        {
            using (LogiqguruEntities context = new LogiqguruEntities())
            {
                return context.Database.SqlQuery<UserswithSubs>("select u.UserId,u.UserName,u.Password,u.Mobile, u.IsActive,su.SubUserId,su.FirstName,su.LastName, su.ClassId, su.ChildImage, su.City, su.School, su.Marksgainedinpretest,su.Marksgainedincurriculumpretest from Tbl_Users as u join Tbl_Subusers as su on u.UserId = su.UserId where su.SubUserId =" + subuserid).FirstOrDefault();
            }
        }
        public bool updateisactiveforloggedin(long Subuserid, long Userid)
        {
            using (LogiqguruEntities context = new LogiqguruEntities())
            {
                Tbl_Subusers subuserdata = context.Tbl_Subusers.Where(x => x.IsActive == true && x.UserId == Userid).FirstOrDefault();
                if (subuserdata != null)
                {
                    subuserdata.IsActive = false;
                    context.SaveChanges();
                }
                Tbl_Subusers subuserdatatoupdate = context.Tbl_Subusers.Where(x => x.SubUserId == Subuserid).FirstOrDefault();
                subuserdatatoupdate.IsActive = true;
                context.SaveChanges();
                return true;

            }
        }
    }
    public class Pretestrepository : RepositoryBase<Tbl_Pretest>
    {
        public List<Questionswithpre> Submitpretest(long Subuserid)
        {
            using (LogiqguruEntities context = new LogiqguruEntities())
            {

                return context.Database.SqlQuery<Questionswithpre>("SELECT *  FROM [dbo].[Tbl_Pretest] as pre join TBl_Questions as q on q.Q_Id=pre.QuestionId where pre.SubUserId=" + Subuserid).ToList();
            }
        }
        public bool Insertpretestdata(long SubUserid, int ClassId)
        {
            List<long> Qids = new List<long>();
            LogiqguruEntities context = new LogiqguruEntities();
            Tbl_Subjects_Info subinfo = context.Tbl_Subjects_Info.Where(x => x.SubjectName == "Pre-Test" && x.ClassId == ClassId).FirstOrDefault();
            Qids = Getquestionidstoinsert(ClassId, subinfo, SubUserid);
            foreach (var item in Qids)
            {
                Tbl_Pretest data = new Tbl_Pretest();
                data.QuestionId = item;
                data.SubUserId = SubUserid;
                Insert(data);
            }
            return true;
        }
        public List<long> Getquestionidstoinsert(int ClassId, Tbl_Subjects_Info subinfo, long Userid)
        {
            LogiqguruEntities context = new LogiqguruEntities();

            Tbl_Chapter_Info chapdatachap1 = context.Tbl_Chapter_Info.Where(x => x.DefaultName == "Chap1" && x.ClassId == ClassId && x.SubjectId == subinfo.SubjectId).FirstOrDefault();
            long[] easyquestionsidentificationchap1 = context.TBl_Questions.Where(x => x.ClassId == ClassId && x.QLevelID == 1 && x.Qstatus == (int)Custom.Status.Approved && x.SubjectId == subinfo.SubjectId && x.ChapterId == chapdatachap1.ChapterId && x.Isactive == true).ToList().Select(r => r.Q_Id).ToArray();
            long[] mediumquestionsidentificationchap1 = context.TBl_Questions.Where(x => x.ClassId == ClassId && x.QLevelID == 2 && x.Qstatus == (int)Custom.Status.Approved && x.SubjectId == subinfo.SubjectId && x.ChapterId == chapdatachap1.ChapterId && x.Isactive == true).ToList().Select(r => r.Q_Id).ToArray();
            long[] toughquestionsidentificationchap1 = context.TBl_Questions.Where(x => x.ClassId == ClassId && x.QLevelID == 3 && x.Qstatus == (int)Custom.Status.Approved && x.SubjectId == subinfo.SubjectId && x.ChapterId == chapdatachap1.ChapterId && x.Isactive == true).ToList().Select(r => r.Q_Id).ToArray();
            Tbl_Chapter_Info chapdatachap2 = context.Tbl_Chapter_Info.Where(x => x.DefaultName == "Chap2" && x.ClassId == ClassId && x.SubjectId == subinfo.SubjectId).FirstOrDefault();
            long[] easyquestionsobservationchap2 = context.TBl_Questions.Where(x => x.ClassId == ClassId && x.QLevelID == 1 && x.Qstatus == (int)Custom.Status.Approved && x.SubjectId == subinfo.SubjectId && x.ChapterId == chapdatachap2.ChapterId && x.Isactive == true).ToList().Select(r => r.Q_Id).ToArray();
            long[] mediumquestionsobservationchap2 = context.TBl_Questions.Where(x => x.ClassId == ClassId && x.QLevelID == 2 && x.Qstatus == (int)Custom.Status.Approved && x.SubjectId == subinfo.SubjectId && x.ChapterId == chapdatachap2.ChapterId && x.Isactive == true).ToList().Select(r => r.Q_Id).ToArray();
            long[] toughquestionsobservationchap2 = context.TBl_Questions.Where(x => x.ClassId == ClassId && x.QLevelID == 3 && x.Qstatus == (int)Custom.Status.Approved && x.SubjectId == subinfo.SubjectId && x.ChapterId == chapdatachap2.ChapterId && x.Isactive == true).ToList().Select(r => r.Q_Id).ToArray();
            Tbl_Chapter_Info chapdatachap3 = context.Tbl_Chapter_Info.Where(x => x.DefaultName == "Chap3" && x.ClassId == ClassId && x.SubjectId == subinfo.SubjectId).FirstOrDefault();
            long[] easyquestionsmemorychap3 = context.TBl_Questions.Where(x => x.ClassId == ClassId && x.QLevelID == 1 && x.Qstatus == (int)Custom.Status.Approved && x.SubjectId == subinfo.SubjectId && x.ChapterId == chapdatachap3.ChapterId && x.Isactive == true).ToList().Select(r => r.Q_Id).ToArray();
            long[] mediumquestionsmemorychap3 = context.TBl_Questions.Where(x => x.ClassId == ClassId && x.QLevelID == 2 && x.Qstatus == (int)Custom.Status.Approved && x.SubjectId == subinfo.SubjectId && x.ChapterId == chapdatachap3.ChapterId && x.Isactive == true).ToList().Select(r => r.Q_Id).ToArray();
            long[] toughquestionsmemorychap3 = context.TBl_Questions.Where(x => x.ClassId == ClassId && x.QLevelID == 3 && x.Qstatus == (int)Custom.Status.Approved && x.SubjectId == subinfo.SubjectId && x.ChapterId == chapdatachap3.ChapterId && x.Isactive == true).ToList().Select(r => r.Q_Id).ToArray();
            long[] easyquestionschap4 = null;
            long[] mediumquestionschap4 = null;
            long[] toughquestionschap4 = null;
            //long[] easyquestionschap5 = null;
            //long[] mediumquestionschap5 = null;
            //long[] toughquestionschap5 = null
            if (ClassId == (int)Custom.classname.Class1 || ClassId == (int)Custom.classname.Class2 || ClassId == (int)Custom.classname.Class3 || ClassId == (int)Custom.classname.Class4 || ClassId == (int)Custom.classname.Class5 || ClassId == (int)Custom.classname.Class6 || ClassId == (int)Custom.classname.Class7 || ClassId == (int)Custom.classname.Class8 || ClassId == (int)Custom.classname.Class9 || ClassId == (int)Custom.classname.Class10)
            {
                Tbl_Chapter_Info chapdatachap4 = context.Tbl_Chapter_Info.Where(x => x.DefaultName == "Chap4" && x.ClassId == ClassId && x.SubjectId == subinfo.SubjectId).FirstOrDefault();

                easyquestionschap4 = context.TBl_Questions.Where(x => x.ClassId == ClassId && x.QLevelID == 1 && x.Qstatus == (int)Custom.Status.Approved && x.SubjectId == subinfo.SubjectId && x.ChapterId == chapdatachap4.ChapterId && x.Isactive == true).ToList().Select(r => r.Q_Id).ToArray();
                mediumquestionschap4 = context.TBl_Questions.Where(x => x.ClassId == ClassId && x.QLevelID == 2 && x.Qstatus == (int)Custom.Status.Approved && x.SubjectId == subinfo.SubjectId && x.ChapterId == chapdatachap4.ChapterId && x.Isactive == true).ToList().Select(r => r.Q_Id).ToArray();
                toughquestionschap4 = context.TBl_Questions.Where(x => x.ClassId == ClassId && x.QLevelID == 3 && x.Qstatus == (int)Custom.Status.Approved && x.SubjectId == subinfo.SubjectId && x.ChapterId == chapdatachap4.ChapterId && x.Isactive == true).ToList().Select(r => r.Q_Id).ToArray();

            }
            //if (ClassId == (int)Custom.classname.Class3 || ClassId == (int)Custom.classname.Class4 || ClassId == (int)Custom.classname.Class5 || ClassId == (int)Custom.classname.Class6 || ClassId == (int)Custom.classname.Class7 || ClassId == (int)Custom.classname.Class8 || ClassId == (int)Custom.classname.Class9 || ClassId == (int)Custom.classname.Class10)
            //{
            //    Tbl_Chapter_Info chapdatachap5 = context.Tbl_Chapter_Info.Where(x => x.DefaultName == "Chap5").FirstOrDefault();

            //    easyquestionschap5 = context.TBl_Questions.Where(x => x.ClassId == ClassId && x.QLevelID == 1 && x.Qstatus == (int)Custom.Status.Approved && x.SubjectId == subinfo.SubjectId && x.ChapterId == chapdatachap5.ChapterId && x.Isactive == true).ToList().Select(r => r.Q_Id).ToArray();
            //     mediumquestionschap5 = context.TBl_Questions.Where(x => x.ClassId == ClassId && x.QLevelID == 2 && x.Qstatus == (int)Custom.Status.Approved && x.SubjectId == subinfo.SubjectId && x.ChapterId == chapdatachap5.ChapterId && x.Isactive == true).ToList().Select(r => r.Q_Id).ToArray();
            //    toughquestionschap5 = context.TBl_Questions.Where(x => x.ClassId == ClassId && x.QLevelID == 3 && x.Qstatus == (int)Custom.Status.Approved && x.SubjectId == subinfo.SubjectId && x.ChapterId == chapdatachap5.ChapterId && x.Isactive == true).ToList().Select(r => r.Q_Id).ToArray();

            //}


            long[] shuffledeasyids = Shuffle<long>(easyquestionsidentificationchap1);
            long[] shuffledmediumids = Shuffle<long>(mediumquestionsidentificationchap1);
            long[] shuffleddifficultids = Shuffle<long>(toughquestionsidentificationchap1);
            long[] shuffledeasyidschap2 = Shuffle<long>(easyquestionsobservationchap2);
            long[] shuffledmediumidschap2 = Shuffle<long>(mediumquestionsobservationchap2);
            long[] shuffleddifficultidschap2 = Shuffle<long>(toughquestionsobservationchap2);
            long[] shuffledeasyidschap3 = Shuffle<long>(easyquestionsmemorychap3);
            long[] shuffledmediumidschap3 = Shuffle<long>(mediumquestionsmemorychap3);
            long[] shuffleddifficultidschap3 = Shuffle<long>(toughquestionsmemorychap3);
            long[] shuffledeasyidschap4 = null;
            long[] shuffledmediumidschap4 = null;
            long[] shuffleddifficultidschap4 = null;
            if (ClassId == (int)Custom.classname.Class1 || ClassId == (int)Custom.classname.Class2 || ClassId == (int)Custom.classname.Class3 || ClassId == (int)Custom.classname.Class4 || ClassId == (int)Custom.classname.Class5 || ClassId == (int)Custom.classname.Class6 || ClassId == (int)Custom.classname.Class7 || ClassId == (int)Custom.classname.Class8 || ClassId == (int)Custom.classname.Class9 || ClassId == (int)Custom.classname.Class10)
            {
                shuffledeasyidschap4 = Shuffle<long>(easyquestionschap4);
                shuffledmediumidschap4 = Shuffle<long>(mediumquestionschap4);
                shuffleddifficultidschap4 = Shuffle<long>(toughquestionschap4);
            }
            List<long> Qids = new List<long>();
            int easych1 = 0;
            int mediumch1 = 0;
            int toughch1 = 0;
            int easych2 = 0;
            int mediumch2 = 0;
            int toughch2 = 0;
            int easych3 = 0;
            int mediumch3 = 0;
            int toughch3 = 0;
            int easych4 = 0;
            int mediumch4 = 0;
            int toughch4 = 0;
            if (ClassId == (int)Custom.classname.Preprimary)
            {
                easych1 = (int)Custom.emtprimary.easych1;
                mediumch1 = (int)Custom.emtprimary.mediumch1;
                toughch1 = (int)Custom.emtprimary.toughch1;
                easych2 = (int)Custom.emtprimary.easych2;
                mediumch2 = (int)Custom.emtprimary.mediumch2;
                toughch2 = (int)Custom.emtprimary.toughch2;
                easych3 = (int)Custom.emtprimary.easych3;
                mediumch3 = (int)Custom.emtprimary.mediumch3;
                toughch3 = (int)Custom.emtprimary.toughch3;
            }
            else
            {
                easych1 = (int)Custom.emtaboveprimary.easych1;
                mediumch1 = (int)Custom.emtaboveprimary.mediumch1;
                toughch1 = (int)Custom.emtaboveprimary.toughch1;
                easych2 = (int)Custom.emtaboveprimary.easych2;
                mediumch2 = (int)Custom.emtaboveprimary.mediumch2;
                toughch2 = (int)Custom.emtaboveprimary.toughch2;
                easych3 = (int)Custom.emtaboveprimary.easych3;
                mediumch3 = (int)Custom.emtaboveprimary.mediumch3;
                toughch3 = (int)Custom.emtaboveprimary.toughch3;
                easych4 = (int)Custom.emtaboveprimary.easych4;
                mediumch4 = (int)Custom.emtaboveprimary.mediumch4;
                toughch4 = (int)Custom.emtaboveprimary.toughch4;
            }

            for (int i = 0; i <= (int)Custom.pretest.total; i++)
            {
                if (i <= easych1)  //3 easy for chap1
                {
                    foreach (var item in shuffledeasyids)
                    {
                        if (context.Tbl_Pretest.Where(x => x.QuestionId == item && x.SubUserId == Userid).FirstOrDefault() == null)
                        {
                            if (!Qids.Contains(item))
                            {
                                Qids.Add(item);
                                break;
                            }
                        }
                    }
                }
                else if (i > easych1 && i <= mediumch1)  //2 medium for chap1
                {
                    foreach (var item in shuffledmediumids)
                    {
                        if (context.Tbl_Pretest.Where(x => x.QuestionId == item && x.SubUserId == Userid).FirstOrDefault() == null)
                        {
                            if (!Qids.Contains(item))
                            {
                                Qids.Add(item);
                                break;
                            }
                        }
                    }
                }
                else if (i > mediumch1 && i <= toughch1)
                {
                    foreach (var item in shuffleddifficultids)
                    {
                        if (context.Tbl_Pretest.Where(x => x.QuestionId == item && x.SubUserId == Userid).FirstOrDefault() == null)
                        {
                            if (!Qids.Contains(item))
                            {
                                Qids.Add(item);
                                break;
                            }
                        }
                    }
                }
                else if (i > toughch1 && i <= easych2)
                {
                    foreach (var item in shuffledeasyidschap2)
                    {
                        if (context.Tbl_Pretest.Where(x => x.QuestionId == item && x.SubUserId == Userid).FirstOrDefault() == null)
                        {
                            if (!Qids.Contains(item))
                            {
                                Qids.Add(item);
                                break;
                            }
                        }
                    }
                }
                else if (i > easych2 && i <= mediumch2)
                {
                    foreach (var item in shuffledmediumidschap2)
                    {
                        if (context.Tbl_Pretest.Where(x => x.QuestionId == item && x.SubUserId == Userid).FirstOrDefault() == null)
                        {
                            if (!Qids.Contains(item))
                            {
                                Qids.Add(item);
                                break;
                            }
                        }
                    }
                }
                else if (i > mediumch2 && i <= toughch2)
                {
                    foreach (var item in shuffleddifficultidschap2)
                    {
                        if (context.Tbl_Pretest.Where(x => x.QuestionId == item && x.SubUserId == Userid).FirstOrDefault() == null)
                        {
                            if (!Qids.Contains(item))
                            {
                                Qids.Add(item);
                                break;
                            }
                        }
                    }
                }
                else if (i > toughch2 && i <= easych3)
                {
                    foreach (var item in shuffledeasyidschap3)
                    {
                        if (context.Tbl_Pretest.Where(x => x.QuestionId == item && x.SubUserId == Userid).FirstOrDefault() == null)
                        {
                            if (!Qids.Contains(item))
                            {
                                Qids.Add(item);
                                break;
                            }
                        }
                    }
                }
                else if (i > easych3 && i <= mediumch3)
                {
                    foreach (var item in shuffledmediumidschap3)
                    {
                        if (context.Tbl_Pretest.Where(x => x.QuestionId == item && x.SubUserId == Userid).FirstOrDefault() == null)
                        {
                            if (!Qids.Contains(item))
                            {
                                Qids.Add(item);
                                break;
                            }
                        }
                    }
                }
                else if (i > mediumch3 && i <= toughch3)
                {
                    foreach (var item in shuffleddifficultidschap3)
                    {
                        if (context.Tbl_Pretest.Where(x => x.QuestionId == item && x.SubUserId == Userid).FirstOrDefault() == null)
                        {
                            if (!Qids.Contains(item))
                            {
                                Qids.Add(item);
                                break;
                            }
                        }
                    }
                }
                else if (i > toughch3 && i <= easych4)
                {
                    foreach (var item in shuffledeasyidschap4)
                    {
                        if (context.Tbl_Pretest.Where(x => x.QuestionId == item && x.SubUserId == Userid).FirstOrDefault() == null)
                        {
                            if (!Qids.Contains(item))
                            {
                                Qids.Add(item);
                                break;
                            }
                        }
                    }
                }
                else if (i > easych4 && i <= mediumch4)
                {
                    foreach (var item in shuffledmediumidschap4)
                    {
                        if (context.Tbl_Pretest.Where(x => x.QuestionId == item && x.SubUserId == Userid).FirstOrDefault() == null)
                        {
                            if (!Qids.Contains(item))
                            {
                                Qids.Add(item);
                                break;
                            }
                        }
                    }
                }
                else if (i > mediumch4 && i <= toughch4)
                {
                    foreach (var item in shuffleddifficultidschap4)
                    {
                        if (context.Tbl_Pretest.Where(x => x.QuestionId == item && x.SubUserId == Userid).FirstOrDefault() == null)
                        {
                            if (!Qids.Contains(item))
                            {
                                Qids.Add(item);
                                break;
                            }
                        }
                    }
                }


            }
            return Qids;
        }
        public T[] Shuffle<T>(T[] array)
        {
            Random _random = new Random();
            var random = _random;
            for (int i = array.Length; i > 1; i--)
            {
                // Pick random element to swap.
                int j = random.Next(i); // 0 <= j <= i-1
                                        // Swap.
                T tmp = array[j];
                array[j] = array[i - 1];
                array[i - 1] = tmp;
            }
            return array;
        }
        public bool Updatepresetwhenanswered(long Pretestid, bool Answer, string selectedanswer, int totalquestions)
        {
            using (LogiqguruEntities context = new LogiqguruEntities())
            {
                Tbl_Pretest datatoupdate = context.Tbl_Pretest.Where(x => x.PreTestID == Pretestid).FirstOrDefault();
                datatoupdate.IsAnswered = true;
                datatoupdate.IsCorrect = Answer;
                datatoupdate.Answered = selectedanswer;
                context.SaveChanges();

                if (totalquestions == context.Tbl_Pretest.Where(x => x.SubUserId == datatoupdate.SubUserId && x.IsAnswered == true).Count())
                {
                    int pretestresult = context.Tbl_Pretest.Where(x => x.SubUserId == datatoupdate.SubUserId && x.IsCorrect == true).Count();

                    Tbl_Subusers updateresult = context.Tbl_Subusers.Where(x => x.SubUserId == datatoupdate.SubUserId).FirstOrDefault();
                    updateresult.Marksgainedinpretest = pretestresult;
                    context.SaveChanges();

                    Worksheetrepository rep = new Worksheetrepository();
                    List<Tbl_Paymenttransactions> ptrans = context.Tbl_Paymenttransactions.Where(x => x.Isworksheetsassigned == false && x.SubUserId == updateresult.SubUserId).ToList();
                    if (ptrans != null)
                    {
                        rep.Insertpaidworksheets(datatoupdate.SubUserId, updateresult.ClassId, ptrans.Sum(x => x.No_of_Worksheetstobeassigned), Convert.ToInt32(ptrans.FirstOrDefault().SubjectId));
                        foreach (var item in ptrans)
                        {
                            item.Isworksheetsassigned = true;
                            context.SaveChanges();

                        }
                    }
                }
            }
            return true;
        }
    }
    public class QuestionsRepository : RepositoryBase<TBl_Questions>
    {
        public List<Questionswithpre> Getquestions(long SubUserId)
        {
            //List<TBl_Questions> questions = new List<TBl_Questions>();
            using (LogiqguruEntities context = new LogiqguruEntities())
            {
                Tbl_Pretest pretest = context.Tbl_Pretest.Where(x => x.SubUserId == SubUserId).FirstOrDefault();
                if (pretest == null)
                {
                    Tbl_Subusers subuserdata = context.Tbl_Subusers.Where(x => x.SubUserId == SubUserId).FirstOrDefault();
                    Pretestrepository rep = new Pretestrepository();
                    rep.Insertpretestdata(SubUserId, subuserdata.ClassId);
                }
                List<Questionswithpre> questions = context.Database.SqlQuery<Questionswithpre>("SELECT *  FROM [dbo].[Tbl_Pretest] as pre join TBl_Questions as q on q.Q_Id=pre.QuestionId where pre.SubUserId=" + SubUserId).ToList();
                //context.Tbl_Pretest.Where(x =>x.SubUserId==SubUserId ).ToList().Select(r => r.QuestionId).ToArray();
                //foreach(var item in Qids)
                //{
                //    questions.Add(Get(x => x.Q_Id == item).FirstOrDefault());
                //}
                return questions;

            }
        }
    }
    public class Worksheetrepository : RepositoryBase<Tbl_Worksheets>
    {
        public List<Worksheethistory> Getworksheethistory(long SubUserid)
        {
            using (LogiqguruEntities context = new LogiqguruEntities())
            {
                return context.Database.SqlQuery<Worksheethistory>("select cq.[WorksheetId], max(c.ChapterName) as ChapterName  , max(cw.Completeddate) as Completeddate, COUNT(cq.IsCorrect) as Marksgainedinthissheet, (COUNT(cq.IsCorrect) * 10) as Accuracy, (COUNT(cq.IsCorrect) * 20) as Coins   FROM[db_Logiqguru].[dbo].[Tbl_worksheetquestions] cq   join[dbo].[Tbl_worksheets]  cw  on cq.[WorksheetId] = cw.[WorksheetId]  and cw.IsCompleted=1   join[dbo].Tbl_Chapter_Info c on c.ChapterId = cw.ChapterId    where cq.IsCorrect = 1  and cw.SubUserId=" + SubUserid + " group by cq.[WorksheetId]").ToList();
            }

        }
        public bool Insertfreeworksheets(long SubUserid, long ClassId)
        {
            using (LogiqguruEntities context = new LogiqguruEntities())
            {
                Tbl_Class_Info classdata = context.Tbl_Class_Info.Where(x => x.ClassId == ClassId).FirstOrDefault();
                int? marksgained = context.Tbl_Subusers.Where(x => x.SubUserId == SubUserid).FirstOrDefault().Marksgainedinpretest;
                Tbl_Subjects_Info subinfo = context.Tbl_Subjects_Info.Where(x => x.SubjectName != "Pre-Test" && x.ClassId == ClassId).FirstOrDefault();
                return Insertworksheets(ClassId, marksgained, subinfo, SubUserid);
            }

        }
        public bool Insertworksheets(long ClassId, int? marksgained, Tbl_Subjects_Info subinfo, long SubUserid)
        {
            using (LogiqguruEntities context = new LogiqguruEntities())
            {

                Tbl_Deff_Dataforworksheet datatocreateworksheets = context.Tbl_Deff_Dataforworksheet.Where(x => x.ClassId == ClassId && x.IsFreeworksheet == true).FirstOrDefault();
                {
                    for (int i = 1; i <= datatocreateworksheets.Chap1; i++)
                    {
                        Tbl_Chapter_Info chapdata = context.Tbl_Chapter_Info.Where(x => x.DefaultName == "Chap1" && x.ClassId == ClassId && x.SubjectId == subinfo.SubjectId).FirstOrDefault();
                        Tbl_Worksheets insertdata = new Tbl_Worksheets();
                        insertdata.ChapterId = chapdata.ChapterId;
                        insertdata.CreatedOn = DateTime.Now;
                        insertdata.SubUserId = SubUserid;
                        insertdata.Status = (int)Custom.Worksheetstatus.Created;
                        insertdata.Tobeopened = false;
                        if (i == 1)
                        {
                            insertdata.Tobeopened = true;
                        }
                        context.Tbl_Worksheets.Add(insertdata);
                        insertdata = null;
                    }
                    for (int i = 1; i <= datatocreateworksheets.Chap2; i++)
                    {
                        Tbl_Chapter_Info chapdata = context.Tbl_Chapter_Info.Where(x => x.DefaultName == "Chap2" && x.ClassId == ClassId && x.SubjectId == subinfo.SubjectId).FirstOrDefault();
                        Tbl_Worksheets insertdata = new Tbl_Worksheets();
                        insertdata.ChapterId = chapdata.ChapterId;
                        insertdata.CreatedOn = DateTime.Now;
                        insertdata.SubUserId = SubUserid;
                        insertdata.Status = (int)Custom.Worksheetstatus.Created;
                        insertdata.Tobeopened = false;
                        if (i == 1)
                        {
                            insertdata.Tobeopened = true;
                        }
                        context.Tbl_Worksheets.Add(insertdata);
                        insertdata = null;

                    }
                    for (int i = 1; i <= datatocreateworksheets.Chap3; i++)
                    {
                        Tbl_Chapter_Info chapdata = context.Tbl_Chapter_Info.Where(x => x.DefaultName == "Chap3" && x.ClassId == ClassId && x.SubjectId == subinfo.SubjectId).FirstOrDefault();
                        Tbl_Worksheets insertdata = new Tbl_Worksheets();
                        insertdata.ChapterId = chapdata.ChapterId;
                        insertdata.CreatedOn = DateTime.Now;
                        insertdata.SubUserId = SubUserid;
                        insertdata.Status = (int)Custom.Worksheetstatus.Created;
                        insertdata.Tobeopened = false;
                        if (i == 1)
                        {

                            insertdata.Tobeopened = true;
                        }
                        context.Tbl_Worksheets.Add(insertdata);
                        insertdata = null;

                    }
                    for (int i = 1; i <= datatocreateworksheets.Chap4; i++)
                    {
                        Tbl_Chapter_Info chapdata = context.Tbl_Chapter_Info.Where(x => x.DefaultName == "Chap4" && x.ClassId == ClassId && x.SubjectId == subinfo.SubjectId).FirstOrDefault();
                        Tbl_Worksheets insertdata = new Tbl_Worksheets();
                        insertdata.ChapterId = chapdata.ChapterId;
                        insertdata.CreatedOn = DateTime.Now;
                        insertdata.SubUserId = SubUserid;
                        insertdata.Status = (int)Custom.Worksheetstatus.Created;
                        insertdata.Tobeopened = false;
                        if (i == 1)
                        {
                            insertdata.Tobeopened = true;
                        }
                        context.Tbl_Worksheets.Add(insertdata);
                        insertdata = null;

                    }
                    for (int i = 1; i <= datatocreateworksheets.Chap5; i++)
                    {
                        Tbl_Chapter_Info chapdata = context.Tbl_Chapter_Info.Where(x => x.DefaultName == "Chap5" && x.ClassId == ClassId && x.SubjectId == subinfo.SubjectId).FirstOrDefault();
                        Tbl_Worksheets insertdata = new Tbl_Worksheets();
                        insertdata.ChapterId = chapdata.ChapterId;
                        insertdata.CreatedOn = DateTime.Now;
                        insertdata.SubUserId = SubUserid;
                        insertdata.Status = (int)Custom.Worksheetstatus.Created;
                        insertdata.Tobeopened = false;
                        if (i == 1)
                        {
                            insertdata.Tobeopened = true;
                        }
                        context.Tbl_Worksheets.Add(insertdata);
                        insertdata = null;

                    }
                    for (int i = 1; i <= datatocreateworksheets.Chap6; i++)
                    {
                        Tbl_Chapter_Info chapdata = context.Tbl_Chapter_Info.Where(x => x.DefaultName == "Chap6" && x.ClassId == ClassId && x.SubjectId == subinfo.SubjectId).FirstOrDefault();
                        Tbl_Worksheets insertdata = new Tbl_Worksheets();
                        insertdata.ChapterId = chapdata.ChapterId;
                        insertdata.CreatedOn = DateTime.Now;
                        insertdata.SubUserId = SubUserid;
                        insertdata.Status = (int)Custom.Worksheetstatus.Created;
                        insertdata.Tobeopened = false;
                        if (i == 1)
                        {
                            insertdata.Tobeopened = true;
                        }
                        context.Tbl_Worksheets.Add(insertdata);
                        insertdata = null;
                    }
                    context.SaveChanges();
                    return true;
                }
            }
        }
        public bool Insertworksheetspaid(long ClassId, int? marksgained, Tbl_Subjects_Info subinfo, long SubUserid, bool isfreeworksheet, int Noofsheets)
        {
            if (subinfo.SubjectName == "LOGIQ")
            {
                using (LogiqguruEntities context = new LogiqguruEntities())
                {

                    Tbl_Deff_Dataforworksheet datatocreateworksheets = context.Tbl_Deff_Dataforworksheet.Where(x => x.ClassId == ClassId && x.IsFreeworksheet == isfreeworksheet && x.Marksgained == marksgained).FirstOrDefault();
                    {
                        if (marksgained.HasValue)
                        {
                            for (int i = 1; i <= datatocreateworksheets.Chap1 * Noofsheets; i++)
                            {
                                Tbl_Chapter_Info chapdata = context.Tbl_Chapter_Info.Where(x => x.DefaultName == "Chap1" && x.ClassId == ClassId && x.SubjectId == subinfo.SubjectId).FirstOrDefault();
                                Tbl_Worksheets insertdata = new Tbl_Worksheets();
                                insertdata.ChapterId = chapdata.ChapterId;
                                insertdata.CreatedOn = DateTime.Now;
                                insertdata.SubUserId = SubUserid;
                                insertdata.Status = (int)Custom.Worksheetstatus.Created;
                                insertdata.Tobeopened = false;
                                if (i == 1)
                                {
                                    if (context.Tbl_Worksheets.Where(x => x.Tobeopened == true && x.IsCompleted == null && x.SubUserId == SubUserid && x.ChapterId == chapdata.ChapterId).FirstOrDefault() == null)
                                    {
                                        insertdata.Tobeopened = true;
                                    }
                                }
                                context.Tbl_Worksheets.Add(insertdata);
                                insertdata = null;
                            }
                            for (int i = 1; i <= datatocreateworksheets.Chap2 * Noofsheets; i++)
                            {
                                Tbl_Chapter_Info chapdata = context.Tbl_Chapter_Info.Where(x => x.DefaultName == "Chap2" && x.ClassId == ClassId && x.SubjectId == subinfo.SubjectId).FirstOrDefault();
                                Tbl_Worksheets insertdata = new Tbl_Worksheets();
                                insertdata.ChapterId = chapdata.ChapterId;
                                insertdata.CreatedOn = DateTime.Now;
                                insertdata.SubUserId = SubUserid;
                                insertdata.Status = (int)Custom.Worksheetstatus.Created;
                                insertdata.Tobeopened = false;
                                if (i == 1)
                                {

                                    if (context.Tbl_Worksheets.Where(x => x.Tobeopened == true && x.IsCompleted == null && x.SubUserId == SubUserid).FirstOrDefault() == null)
                                    {
                                        insertdata.Tobeopened = true;
                                    }
                                }
                                context.Tbl_Worksheets.Add(insertdata);
                                insertdata = null;

                            }
                            for (int i = 1; i <= datatocreateworksheets.Chap3 * Noofsheets; i++)
                            {
                                Tbl_Chapter_Info chapdata = context.Tbl_Chapter_Info.Where(x => x.DefaultName == "Chap3" && x.ClassId == ClassId && x.SubjectId == subinfo.SubjectId).FirstOrDefault();
                                Tbl_Worksheets insertdata = new Tbl_Worksheets();
                                insertdata.ChapterId = chapdata.ChapterId;
                                insertdata.CreatedOn = DateTime.Now;
                                insertdata.SubUserId = SubUserid;
                                insertdata.Status = (int)Custom.Worksheetstatus.Created;
                                insertdata.Tobeopened = false;
                                if (i == 1)
                                {
                                    if (context.Tbl_Worksheets.Where(x => x.Tobeopened == true && x.IsCompleted == null && x.SubUserId == SubUserid && x.ChapterId == chapdata.ChapterId).FirstOrDefault() == null)
                                    {
                                        insertdata.Tobeopened = true;
                                    }
                                }
                                context.Tbl_Worksheets.Add(insertdata);
                                insertdata = null;

                            }
                            for (int i = 1; i <= datatocreateworksheets.Chap4 * Noofsheets; i++)
                            {
                                Tbl_Chapter_Info chapdata = context.Tbl_Chapter_Info.Where(x => x.DefaultName == "Chap4" && x.ClassId == ClassId).FirstOrDefault();
                                Tbl_Worksheets insertdata = new Tbl_Worksheets();
                                insertdata.ChapterId = chapdata.ChapterId;
                                insertdata.CreatedOn = DateTime.Now;
                                insertdata.SubUserId = SubUserid;
                                insertdata.Status = (int)Custom.Worksheetstatus.Created;
                                insertdata.Tobeopened = false;
                                if (i == 1)
                                {
                                    if (context.Tbl_Worksheets.Where(x => x.Tobeopened == true && x.IsCompleted == null && x.SubUserId == SubUserid && x.ChapterId == chapdata.ChapterId).FirstOrDefault() == null)
                                    {
                                        insertdata.Tobeopened = true;
                                    }
                                }
                                context.Tbl_Worksheets.Add(insertdata);
                                insertdata = null;

                            }
                            for (int i = 1; i <= datatocreateworksheets.Chap5 * Noofsheets; i++)
                            {
                                Tbl_Chapter_Info chapdata = context.Tbl_Chapter_Info.Where(x => x.DefaultName == "Chap5" && x.ClassId == ClassId).FirstOrDefault();
                                Tbl_Worksheets insertdata = new Tbl_Worksheets();
                                insertdata.ChapterId = chapdata.ChapterId;
                                insertdata.CreatedOn = DateTime.Now;
                                insertdata.SubUserId = SubUserid;
                                insertdata.Status = (int)Custom.Worksheetstatus.Created;
                                insertdata.Tobeopened = false;
                                if (i == 1)
                                {
                                    if (context.Tbl_Worksheets.Where(x => x.Tobeopened == true && x.IsCompleted == null && x.SubUserId == SubUserid && x.ChapterId == chapdata.ChapterId).FirstOrDefault() == null)
                                    {
                                        insertdata.Tobeopened = true;
                                    }
                                }
                                context.Tbl_Worksheets.Add(insertdata);
                                insertdata = null;

                            }
                            for (int i = 1; i <= datatocreateworksheets.Chap6 * Noofsheets; i++)
                            {
                                Tbl_Chapter_Info chapdata = context.Tbl_Chapter_Info.Where(x => x.DefaultName == "Chap6" && x.ClassId == ClassId).FirstOrDefault();
                                Tbl_Worksheets insertdata = new Tbl_Worksheets();
                                insertdata.ChapterId = chapdata.ChapterId;
                                insertdata.CreatedOn = DateTime.Now;
                                insertdata.SubUserId = SubUserid;
                                insertdata.Status = (int)Custom.Worksheetstatus.Created;
                                insertdata.Tobeopened = false;
                                if (i == 1)
                                {
                                    if (context.Tbl_Worksheets.Where(x => x.Tobeopened == true && x.IsCompleted == null && x.SubUserId == SubUserid && x.ChapterId == chapdata.ChapterId).FirstOrDefault() == null)
                                    {
                                        insertdata.Tobeopened = true;
                                    }
                                }
                                context.Tbl_Worksheets.Add(insertdata);
                                insertdata = null;
                            }

                            context.SaveChanges();

                            return true;

                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }

            else
            {
                using (LogiqguruEntities context = new LogiqguruEntities())
                {
                    int y = 1;
                    int z = 1;
                    int assesmentcounter = 1;
                    List<Tbl_Chapter_Info> chapdata = context.Tbl_Chapter_Info.Where(x => x.ClassId == ClassId && x.SubjectId == subinfo.SubjectId && x.IsAssessment == null).ToList();
                    foreach (var item in chapdata)
                    {

                        if (assesmentcounter % 3 == 0)
                        {
                            Tbl_Chapter_Info assesmentchapdata = context.Tbl_Chapter_Info.Where(x => x.ClassId == ClassId && x.SubjectId == subinfo.SubjectId && x.DefaultName == "Chap" + z).FirstOrDefault();
                            if (assesmentchapdata != null)
                            {
                                Tbl_Curriculamworksheets datatoinsert = new Tbl_Curriculamworksheets();
                                datatoinsert.ChapterId = assesmentchapdata.ChapterId;
                                datatoinsert.Examtype = 4;
                                datatoinsert.IsVideoopened = false;
                                datatoinsert.SubUserId = SubUserid;
                                context.Tbl_Curriculamworksheets.Add(datatoinsert);
                                context.SaveChanges();
                                z++;
                                assesmentcounter++;
                            }

                        }
                        for (int i = 1; i <= 3; i++)
                        {

                            Tbl_Curriculamworksheets datatoinsert = new Tbl_Curriculamworksheets();
                            if (i <= 1 && y <= 1)
                            {
                                datatoinsert.Tobeopened = true;
                            }
                            datatoinsert.ChapterId = item.ChapterId;
                            datatoinsert.Examtype = i;
                            datatoinsert.IsVideoopened = false;
                            datatoinsert.SubUserId = SubUserid;
                            context.Tbl_Curriculamworksheets.Add(datatoinsert);
                            context.SaveChanges();
                            y++;
                        }
                        assesmentcounter++;

                    }
                    return true;
                }
            }
        }
        public bool Insertworksheetquestions(long WorksheetId, long ClassId, long SubUserId, int chapterid)
        {
            using (LogiqguruEntities context = new LogiqguruEntities())
            {
                Tbl_Worksheets previousworsheetdata = context.Tbl_Worksheets.Where(x => x.IsCompleted == true && x.SubUserId == SubUserId && x.ChapterId == chapterid).OrderByDescending(q => q.WorksheetId).FirstOrDefault();

                int checkdataexist = 0;
                if (previousworsheetdata != null)
                {
                    checkdataexist = context.Tbl_Worksheetquestions.Where(x => x.WorksheetId == previousworsheetdata.WorksheetId).Count();
                }
                int? marksgained = 0;
                Deff_Createquestionsbasedonmarks dataaccordingtomarksgained = new Deff_Createquestionsbasedonmarks();
                if (checkdataexist == 0)
                {
                    marksgained = context.Tbl_Subusers.Where(x => x.SubUserId == SubUserId).FirstOrDefault().Marksgainedinpretest;
                    dataaccordingtomarksgained = context.Deff_Createquestionsbasedonmarks.Where(x => x.Marksgained == marksgained && x.Isfirstworksheet == true).FirstOrDefault();
                }
                else
                {
                    marksgained = previousworsheetdata.MarksGained;
                    dataaccordingtomarksgained = context.Deff_Createquestionsbasedonmarks.Where(x => x.Marksgained == marksgained && x.Isfirstworksheet == false).FirstOrDefault();
                }
                long[] easyquestions = getquestionsbasedonlevel(ClassId, WorksheetId, SubUserId, 1);
                long[] mediumquestions = getquestionsbasedonlevel(ClassId, WorksheetId, SubUserId, 2);
                long[] toughquestions = getquestionsbasedonlevel(ClassId, WorksheetId, SubUserId, 3);
                Pretestrepository rep = new Pretestrepository();

                long[] shuffledeasyids = rep.Shuffle<long>(easyquestions);
                long[] shuffledmediumids = rep.Shuffle<long>(mediumquestions);
                long[] shuffleddifficultids = rep.Shuffle<long>(toughquestions);
                List<long> Qids = new List<long>();
                List<Tuple<long, long>> Cids = new List<Tuple<long, long>>();
                List<Tbl_Worksheetquestions> questionsofbefore = null;
                if (previousworsheetdata != null)
                {
                    questionsofbefore = context.Tbl_Worksheetquestions.Where(x => x.WorksheetId == previousworsheetdata.WorksheetId).ToList();
                }
                int counter = 0;
                if (marksgained != null)
                {
                    for (int i = 1; i <= dataaccordingtomarksgained.Numberofeasy; i++)
                    {
                        foreach (var item in shuffledeasyids)
                        {
                            if (checkifalreadyqnaskedforuser(item, SubUserId) == 0)
                            {
                                if (questionsofbefore != null)
                                {
                                    if (!Qids.Contains(item))
                                    {
                                        if (questionsofbefore[counter].IsCorrect == true)
                                        {
                                            Qids.Add(item);
                                            counter++;
                                            break;
                                        }
                                        else
                                        {
                                            int? moduleid = Getmoduleid(questionsofbefore[counter].Q_Id);
                                            TBl_Questions similarqn = Getsimilarquesionbasedonmoduleid(moduleid, questionsofbefore[counter].Q_Id);
                                            //Cids.Add(new Tuple<long, long>(item, questionsofbefore[counter].Q_Id));
                                            Qids.Add(similarqn.Q_Id);
                                            counter++;
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    if (!Qids.Contains(item))
                                    {
                                        Qids.Add(item);
                                        counter++;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    for (int i = 1; i <= dataaccordingtomarksgained.Numberofmedium; i++)
                    {
                        foreach (var item in shuffledmediumids)
                        {
                            if (checkifalreadyqnaskedforuser(item, SubUserId) == 0)
                            {
                                if (questionsofbefore != null)
                                {
                                    if (!Qids.Contains(item))
                                    {
                                        if (questionsofbefore[counter].IsCorrect == true)
                                        {
                                            Qids.Add(item);
                                            counter++;
                                            break;
                                        }
                                        else
                                        {
                                            Qids.Add(item);
                                            Cids.Add(new Tuple<long, long>(item, questionsofbefore[counter].Q_Id));

                                            counter++;
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    if (!Qids.Contains(item))
                                    {
                                        Qids.Add(item);
                                        counter++;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    for (int i = 1; i <= dataaccordingtomarksgained.Numberoftough; i++)
                    {
                        foreach (var item in shuffleddifficultids)
                        {
                            if (checkifalreadyqnaskedforuser(item, SubUserId) == 0)
                            {
                                if (questionsofbefore != null)
                                {
                                    if (!Qids.Contains(item))
                                    {
                                        if (questionsofbefore[counter].IsCorrect == true)
                                        {
                                            Qids.Add(item);
                                            counter++;
                                            break;
                                        }
                                        else
                                        {
                                            //Tbl_CloneQuestions cq = context.Tbl_CloneQuestions.Where(x => x.Q_Id == questionsofbefore[i].Q_Id).FirstOrDefault();
                                            //if (cq != null)
                                            //{
                                            //    Cids.Add(cq.CloneQ_Id);
                                            //}
                                            //else
                                            //{
                                            //    Qids.Add(item);
                                            //    break;
                                            //}
                                            Qids.Add(item);
                                            Cids.Add(new Tuple<long, long>(item, questionsofbefore[counter].Q_Id));

                                            counter++;
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    if (!Qids.Contains(item))
                                    {
                                        Qids.Add(item);
                                        counter++;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    int totalwustionstobegenerated = (dataaccordingtomarksgained.Numberofeasy + dataaccordingtomarksgained.Numberofmedium + dataaccordingtomarksgained.Numberoftough);
                    if (totalwustionstobegenerated == Qids.Count())
                    {
                        foreach (var item in Qids)
                        {
                            Tbl_Worksheetquestions data = new Tbl_Worksheetquestions();
                            data.Q_Id = item;
                            data.SubUserId = SubUserId;
                            data.CreatedOn = DateTime.Now;
                            data.IsCloneenabled = false;

                            if (Cids.Where(x => x.Item1 == item).FirstOrDefault() != null)
                            {
                                data.IsCloneenabled = true;
                                data.DuplicateQ_Id = Cids.Where(x => x.Item1 == item).FirstOrDefault().Item2;
                            }
                            data.WorksheetId = WorksheetId;
                            context.Tbl_Worksheetquestions.Add(data);
                        }
                        context.SaveChanges();
                    }

                }
                //foreach (var item in Cids)
                //{
                //    Tbl_Worksheetquestions data = new Tbl_Worksheetquestions();
                //    data.CloneQ_Id = item;
                //    data.IsCloneenabled = true;
                //    data.SubUserId = SubUserId;
                //    data.CreatedOn = DateTime.Now;
                //    data.WorksheetId = WorksheetId;

                //    context.Tbl_Worksheetquestions.Add(data);
                //}
                return true;
            }
        }

        private TBl_Questions Getsimilarquesionbasedonmoduleid(int? moduleid, long q_Id)
        {
            using (LogiqguruEntities context = new LogiqguruEntities())
            {
                return context.TBl_Questions.Where(x => x.ModuleId == moduleid && x.Q_Id != q_Id).FirstOrDefault();

            }
        }

        private int? Getmoduleid(long q_Id)
        {
            using (LogiqguruEntities context = new LogiqguruEntities())
            {
                return context.TBl_Questions.Where(x => x.Q_Id == q_Id).FirstOrDefault().ModuleId;
            }
        }

        public int checkifalreadyqnaskedforuser(long Q_Id, long SubUserId)
        {
            using (LogiqguruEntities context = new LogiqguruEntities())
            {
                return context.Tbl_Worksheetquestions.Where(x => x.Q_Id == Q_Id && x.SubUserId == SubUserId).Count();
            }
        }
        public long[] getquestionsbasedonlevel(long ClassId, long WorksheetId, long SubUserId, int level)
        {
            using (LogiqguruEntities context = new LogiqguruEntities())
            {
                Tbl_Worksheets currentworsheetdata = context.Tbl_Worksheets.Where(x => x.WorksheetId == WorksheetId && x.SubUserId == SubUserId).FirstOrDefault();

                Tbl_Subjects_Info subinfo = context.Tbl_Subjects_Info.Where(x => x.SubjectName != "Pre-Test" && x.ClassId == ClassId).FirstOrDefault();
                Tbl_Chapter_Info chapdata = context.Tbl_Chapter_Info.Where(x => x.ChapterId == currentworsheetdata.ChapterId && x.ClassId == ClassId).FirstOrDefault();
                return context.TBl_Questions.Where(x => x.ClassId == ClassId && x.QLevelID == level && x.Qstatus == (int)Custom.Status.Approved && x.SubjectId == subinfo.SubjectId && x.ChapterId == chapdata.ChapterId && x.Isactive == true).ToList().Select(r => r.Q_Id).ToArray();
            }
        }
        public List<Questionswithworksheet> Getquestionsofworksheet(long Worksheetid)
        {
            using (LogiqguruEntities context = new LogiqguruEntities())
            {
                //Tbl_Worksheets updateresult = context.Tbl_Worksheets.Where(x => x.WorksheetId == Worksheetid).FirstOrDefault();
                //updateresult.Status = (int)Custom.Worksheetstatus.Opened;
                //context.SaveChanges();
                return context.Database.SqlQuery<Questionswithworksheet>("SELECT q.Objective,s.SubTopicName, ql.Text,m.ModuleName,wq.*,q.*,c.CloneQ_Id,c.Q_Id as CQ_Id,c.Answer as CAnswer,c.Question as CQuestion, c.OptionA as COptionA,c.OPtionB as COptionB,c.OptionC as COptionC,c.OptionD as COptionD,c.Sloution as CSolution FROM [dbo].[Tbl_Worksheetquestions] as wq join dbo.TBl_Questions as q on q.q_id=wq.q_id left join dbo.Tbl_CloneQuestions as c on c.CloneQ_Id=wq.CloneQ_Id left join dbo.Tbl_Modules as m on m.ModulesId=q.ModuleId left join dbo.Deff_QueationLevel as ql on ql.QLevelID=q.QLevelID left join dbo.Tbl_SubTopics_info as s on s.SubTopicID=q.SubTopicId where wq.Worksheetid=" + Worksheetid).ToList();
            }
        }
        public List<Tbl_Worksheets> Getworksheet(long SubUserId, int chapterid)
        {
            using (LogiqguruEntities context = new LogiqguruEntities())
            {
                return context.Tbl_Worksheets.Where(x => x.SubUserId == SubUserId && x.ChapterId == chapterid).ToList();
            }
        }
        public bool Updateworksheetwhenanswered(long Wqid, bool Answer, string selectedanswer, int totalquestions)
        {
            using (LogiqguruEntities context = new LogiqguruEntities())
            {
                Tbl_Worksheetquestions datatoupdate = context.Tbl_Worksheetquestions.Where(x => x.WQId == Wqid).FirstOrDefault();
                datatoupdate.IsAnswered = true;
                datatoupdate.IsCorrect = Answer;
                datatoupdate.Answergivenbyuser = selectedanswer;
                datatoupdate.AnsweredOn = DateTime.Now;
                context.SaveChanges();

                if (totalquestions == context.Tbl_Worksheetquestions.Where(x => x.WorksheetId == datatoupdate.WorksheetId && x.IsAnswered == true).Count())
                {
                    int worksheetresult = context.Tbl_Worksheetquestions.Where(x => x.WorksheetId == datatoupdate.WorksheetId && x.IsCorrect == true).Count();
                    Tbl_Worksheets updateresult = Get().Where(x => x.WorksheetId == datatoupdate.WorksheetId).FirstOrDefault();
                    updateresult.MarksGained = worksheetresult;
                    updateresult.Status = (int)Custom.Worksheetstatus.Completed;
                    updateresult.IsCompleted = true;
                    updateresult.Completeddate = DateTime.Now;
                    Update(updateresult);
                    Tbl_Worksheets nextworksheettobeopened = Get().Where(x => x.Status == (int)Custom.Worksheetstatus.Created && x.IsCompleted == null && x.SubUserId == updateresult.SubUserId && x.ChapterId == updateresult.ChapterId).FirstOrDefault();
                    nextworksheettobeopened.Tobeopened = true;
                    Update(nextworksheettobeopened);
                    UpdateAwardsrepository uarep = new UpdateAwardsrepository();
                    uarep.Update(worksheetresult, datatoupdate.SubUserId.Value);
                    if (worksheetresult >= 5)
                    {
                        StreakRepository srep = new StreakRepository();
                        srep.Checkandupdatedailygoal(datatoupdate.SubUserId.Value);
                    }
                }
            }
            return true;
        }
        public List<Questionswithworksheet> Submitworksheet(long Worksheetid, long SubUserId, int chapterid)
        {
            using (LogiqguruEntities context = new LogiqguruEntities())
            {
                //int worksheetresult = context.Tbl_Worksheetquestions.Where(x => x.WorksheetId == Worksheetid && x.IsCorrect == true).Count();
                //Tbl_Worksheets updateresult = context.Tbl_Worksheets.Where(x => x.WorksheetId == Worksheetid).FirstOrDefault();
                //updateresult.MarksGained = worksheetresult;
                //updateresult.Status = (int)Custom.Worksheetstatus.Completed;
                //updateresult.IsCompleted = true;
                //context.SaveChanges();
                //Tbl_Worksheets nextworksheettobeopened = context.Tbl_Worksheets.Where(x => x.Status == (int)Custom.Worksheetstatus.Created && x.IsCompleted == null && x.SubUserId == SubUserId && x.ChapterId == chapterid).FirstOrDefault();
                //nextworksheettobeopened.Tobeopened = true;
                //context.SaveChanges();

                return context.Database.SqlQuery<Questionswithworksheet>("SELECT q.Objective,s.SubTopicName, ql.Text,m.ModuleName,wq.*,q.*,(select  (Cast (A AS decimal(10,2)) / Cast (B AS decimal(10,2))*100) from (select count(*) as A from dbo.Tbl_Worksheetquestions where Q_Id = q.q_id and IsCorrect = 1) X join (select count(*) as B from dbo.Tbl_Worksheetquestions where Q_Id = q.q_id) Y on 1 = 1) as TotalValue,c.CloneQ_Id,c.Q_Id as CQ_Id,c.Answer as CAnswer,c.Question as CQuestion, c.OptionA as COptionA,c.OPtionB as COptionB,c.OptionC as COptionC,c.OptionD as COptionD,c.Sloution as CSolution FROM [dbo].[Tbl_Worksheetquestions] as wq join dbo.TBl_Questions as q on q.q_id=wq.q_id left join dbo.Tbl_CloneQuestions as c on c.CloneQ_Id=wq.CloneQ_Id left join dbo.Tbl_Modules as m on m.ModulesId=q.ModuleId left join dbo.Deff_QueationLevel as ql on ql.QLevelID=q.QLevelID left join dbo.Tbl_SubTopics_info as s on s.SubTopicID=q.SubTopicId  where wq.Worksheetid=" + Worksheetid).ToList();
            }
        }
        public List<Questionswithworksheet> Worksheetsummary(long Worksheetid)
        {
            using (LogiqguruEntities context = new LogiqguruEntities())
            {
                //int worksheetresult = context.Tbl_Worksheetquestions.Where(x => x.WorksheetId == Worksheetid && x.IsCorrect == true).Count();
                //Tbl_Worksheets updateresult = context.Tbl_Worksheets.Where(x => x.WorksheetId == Worksheetid).FirstOrDefault();
                //updateresult.MarksGained = worksheetresult;
                //context.SaveChanges();
                return context.Database.SqlQuery<Questionswithworksheet>("SELECT q.Objective,s.SubTopicName, ql.Text,m.ModuleName,wq.*,q.*,c.CloneQ_Id,c.Q_Id as CQ_Id,c.Answer as CAnswer,c.Question as CQuestion, c.OptionA as COptionA,c.OPtionB as COptionB,c.OptionC as COptionC,c.OptionD as COptionD,c.Sloution as CSolution FROM [dbo].[Tbl_Worksheetquestions] as wq join dbo.TBl_Questions as q on q.q_id=wq.q_id left join dbo.Tbl_CloneQuestions as c on c.CloneQ_Id=wq.CloneQ_Id left join dbo.Tbl_Modules as m on m.ModulesId=q.ModuleId left join dbo.Deff_QueationLevel as ql on ql.QLevelID=q.QLevelID left join dbo.Tbl_SubTopics_info as s on s.SubTopicID=q.SubTopicId where wq.Worksheetid=" + Worksheetid).ToList();
            }
        }
        public Questionswithworksheet Fetchclonequestion(long Q_Id, long SubUserId, long WQId, long Duplicateqid)
        {
            using (LogiqguruEntities context = new LogiqguruEntities())
            {
                //Tbl_Worksheetquestions dataofcurrentworksheet = context.Tbl_Worksheetquestions.Where(x => x.WQId == WQId).FirstOrDefault();
                Questionswithworksheet mainquestiondata = Getmainquestion(WQId, SubUserId, Duplicateqid);
                if (mainquestiondata != null)
                {
                    return mainquestiondata;
                }
                return context.Database.SqlQuery<Questionswithworksheet>("SELECT *  FROM  [dbo].[Tbl_Worksheetquestions] as wq  join TBl_Questions as q on q.q_id=wq.q_id where wq.WQId=" + WQId).FirstOrDefault();
            }
        }
        public Questionswithworksheet Getmainquestion(long WQId, long SubUserId, long Duplicateqid)
        {
            using (LogiqguruEntities context = new LogiqguruEntities())
            {

                long cloneid = 0;
                Tbl_Worksheetquestions dataofcurrentworksheet = context.Tbl_Worksheetquestions.Where(x => x.WQId == WQId).FirstOrDefault();//getting data of current wrong question
                Tbl_Worksheets wd = context.Tbl_Worksheets.Where(x => x.WorksheetId == dataofcurrentworksheet.WorksheetId).FirstOrDefault();
                List<Tbl_Worksheets> totalworksheetsofthisuser = context.Tbl_Worksheets.Where(x => x.SubUserId == SubUserId && x.IsCompleted == true && x.ChapterId == wd.ChapterId).ToList();//total worksheets of current user
                Tbl_Worksheetquestions dataofcurrentquestion = null;
                foreach (var item in totalworksheetsofthisuser)
                {
                    List<Tbl_Worksheetquestions> worksheets = context.Tbl_Worksheetquestions.Where(x => x.WorksheetId == item.WorksheetId).ToList();
                    dataofcurrentquestion = worksheets.Where(x => x.Q_Id == Duplicateqid).FirstOrDefault();
                    if (dataofcurrentquestion != null)
                    {
                        for (int i = 1; i <= 15; i++)
                        {
                            if (dataofcurrentquestion != null)
                            {
                                if (dataofcurrentquestion.IsCloneenabled == true)
                                {
                                    dataofcurrentquestion = worksheets.Where(x => x.Q_Id == dataofcurrentquestion.DuplicateQ_Id).FirstOrDefault();
                                }
                                else
                                {
                                    break;
                                }
                            }
                        }
                    }
                }
                if (dataofcurrentquestion != null)
                {
                    List<Tbl_CloneQuestions> getclonequestions = context.Tbl_CloneQuestions.Where(x => x.Q_Id == dataofcurrentquestion.Q_Id).ToList();
                    if (getclonequestions.Count() != 0)
                    {
                        foreach (var itemclones in getclonequestions)
                        {
                            if (context.Tbl_Worksheetquestions.Where(x => x.CloneQ_Id == itemclones.CloneQ_Id && x.SubUserId == SubUserId && x.IsAnswered != null).Count() == 0)
                            {
                                cloneid = itemclones.CloneQ_Id;
                            }
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
                if (cloneid != 0)
                {
                    Tbl_CloneQuestions finaldata = context.Tbl_CloneQuestions.Where(x => x.CloneQ_Id == cloneid).FirstOrDefault();
                    Questionswithworksheet data = new Questionswithworksheet();
                    data.CloneQ_Id = finaldata.CloneQ_Id;
                    data.Question = finaldata.Question;
                    data.OptionA = finaldata.OptionA;
                    data.OPtionB = finaldata.OPtionB;
                    data.OptionC = finaldata.OptionC;
                    data.OptionD = finaldata.OptionD;
                    data.Q_Id = dataofcurrentworksheet.Q_Id;
                    data.Sloution = finaldata.Sloution;
                    data.SubUserId = SubUserId;
                    data.WQId = WQId;
                    data.Answer = finaldata.Answer;
                    data.WorksheetId = dataofcurrentworksheet.WorksheetId;
                    Tbl_Worksheetquestions questiontoupdatecloneid = context.Tbl_Worksheetquestions.Where(x => x.WQId == WQId).FirstOrDefault();
                    if (questiontoupdatecloneid.CloneQ_Id == null)
                    {
                        questiontoupdatecloneid.CloneQ_Id = finaldata.CloneQ_Id;
                        context.SaveChanges();
                    }
                    return data;
                }
                else
                {
                    return null;
                }
            }

        }
        public List<Chapterdata> Getchaptersofthisuser(long SubUserId)
        {
            using (LogiqguruEntities context = new LogiqguruEntities())
            {
                List<Chapterdata> chapterdata = new List<Chapterdata>();
                List<long> chapterid = context.Database.SqlQuery<long>("SELECT [ChapterId]   FROM [dbo].[Tbl_Worksheets] where SubUserId=" + SubUserId + "  group by chapterid ").ToList();
                foreach (var item in chapterid)
                {
                    Tbl_Chapter_Info chapterdatafromdb = context.Tbl_Chapter_Info.Where(x => x.ChapterId == item).FirstOrDefault();
                    Chapterdata cdata = new Chapterdata();
                    cdata.ChapterId = chapterdatafromdb.ChapterId;
                    cdata.Chaptername = chapterdatafromdb.ChapterName;
                    chapterdata.Add(cdata);
                }
                return chapterdata;
            }
        }

        public bool Insertpaidworksheets(long SubUserid, long ClassId, int Noofworksheets, int SubjectId)
        {
            using (LogiqguruEntities context = new LogiqguruEntities())
            {
                Tbl_Class_Info classdata = context.Tbl_Class_Info.Where(x => x.ClassId == ClassId).FirstOrDefault();
                int? marksgained = context.Tbl_Subusers.Where(x => x.SubUserId == SubUserid).FirstOrDefault().Marksgainedinpretest;
                Tbl_Subjects_Info subinfo = context.Tbl_Subjects_Info.Where(x => x.SubjectId == SubjectId && x.ClassId == ClassId).FirstOrDefault();
                bool isfreeworksheet = false;
                return Insertworksheetspaid(ClassId, marksgained, subinfo, SubUserid, isfreeworksheet, Noofworksheets);
            }

        }

        public Dashboard Getdashboarddata(long subuserid)
        {
            Dashboard data = new Dashboard();
            data.Totalworksheets = Get().Where(x => x.SubUserId == subuserid).Count();
            data.Pendingworksheets = Get().Where(x => x.SubUserId == subuserid && x.IsCompleted == null).Count();
            data.Completedworksheets = Get().Where(x => x.SubUserId == subuserid && x.IsCompleted == true).Count();
            decimal? Totalmarksgained = Get().Where(x => x.IsCompleted == true && x.SubUserId == subuserid).Sum(x => x.MarksGained);
            decimal Totalmarks = data.Completedworksheets * 10;
            if (Totalmarksgained.Value != 0)
            {
                data.Totalprofiency = (int)((Totalmarksgained.Value / Totalmarks) * 100);
            }
            data.Worksheets = Get().Where(x => x.IsCompleted == true && x.SubUserId == subuserid).ToList();
            using (LogiqguruEntities context = new LogiqguruEntities())
            {
                Tbl_Availablecoins cdata = context.Tbl_Availablecoins.Where(x => x.SubUserId == subuserid).FirstOrDefault();
                if (cdata != null)
                {
                    data.Totalcoins = cdata.Totalcoins;
                    data.Totaldiamonds = cdata.TotalDiamonds;
                }
                else
                {
                    data.Totalcoins = "0";
                    data.Totaldiamonds = "0";

                }
            }
            return data;
        }
    }
    public class Packagerepository : RepositoryBase<Tbl_Userpackages>
    {
        public List<Tbl_Userpackages> Getpackages(long classid)
        {
            using (LogiqguruEntities context = new LogiqguruEntities())
            {
                Tbl_Subjects_Info data = context.Tbl_Subjects_Info.Where(x => x.ClassId == classid && x.SubjectName == "Logiq").FirstOrDefault();
                return Get(x => x.ClassId == classid && x.IsActive == true && x.SubjectId == data.SubjectId).ToList();
            }
        }
        public List<Tbl_Userpackages> Getcpackages(long classid)
        {
            using (LogiqguruEntities context = new LogiqguruEntities())
            {
                Tbl_Subjects_Info data = context.Tbl_Subjects_Info.Where(x => x.ClassId == classid && x.SubjectName == "curriculum").FirstOrDefault();
                return Get(x => x.ClassId == classid && x.IsActive == true && x.SubjectId == data.SubjectId).ToList();
            }
        }

    }
    public class Paymentrepoitory : RepositoryBase<Tbl_Paymenttransactions>
    {
        public bool Checkpackageexist(long SubUserId)
        {
            Tbl_Paymenttransactions data = Get().Where(x => x.SubUserId == SubUserId).FirstOrDefault();
            using (LogiqguruEntities context = new LogiqguruEntities())
            {
                if (data != null)
                {
                    Tbl_Subjects_Info subdata = context.Tbl_Subjects_Info.Where(x => x.SubjectId == data.SubjectId).FirstOrDefault();
                    if (subdata.SubjectName == "curriculum")
                        return true;
                    else
                        return false;
                }
                return false;

            }
        }
        public bool Checkpackageexistworksheet(long SubUserId)
        {
            List<Tbl_Paymenttransactions> data = Get().Where(x => x.SubUserId == SubUserId).ToList();
            using (LogiqguruEntities context = new LogiqguruEntities())
            {
                if (data != null)
                {
                    foreach (var item in data)
                    {
                        Tbl_Subjects_Info subdata = context.Tbl_Subjects_Info.Where(x => x.SubjectId == item.SubjectId).FirstOrDefault();
                        if (subdata.SubjectName == "LOGIQ")
                        {

                            return true;
                            break;
                        }
                    }
                }
                return false;

            }
        }
        public long Pendingpayment(decimal Amount, int Noofsheets, long SubUserId, int SubjectId)
        {
            Tbl_Paymenttransactions ptrans = new Tbl_Paymenttransactions();
            ptrans.Createdon = DateTime.Now;
            ptrans.Isworksheetsassigned = false;
            ptrans.No_of_Worksheetstobeassigned = Noofsheets;
            ptrans.PaymenttransactionId = "";
            ptrans.Status = (int)Custom.PaymentStatus.Pending;
            ptrans.SubUserId = SubUserId;
            ptrans.Amount = Convert.ToInt16(Amount);
            ptrans.Message = Enum.GetName(typeof(Custom.PaymentStatus), (int)Custom.PaymentStatus.Pending);
            ptrans.SubjectId = SubjectId;
            Insert(ptrans);
            return ptrans.Id;
        }

        public int Successpayment(long Id, long ClassId, long SubUserId)
        {
            Tbl_Paymenttransactions ptrans = Get().Where(x => x.Id == Id).FirstOrDefault();
            ptrans.Createdon = DateTime.Now;
            //ptrans.Isworksheetsassigned = false;
            //ptrans.No_of_Worksheetstobeassigned = Noofsheets;
            ptrans.PaymenttransactionId = "11";
            ptrans.Status = (int)Custom.PaymentStatus.Success;
            ptrans.Message = Enum.GetName(typeof(Custom.PaymentStatus), (int)Custom.PaymentStatus.Success);
            Worksheetrepository wrep = new Worksheetrepository();
            bool status = wrep.Insertpaidworksheets(SubUserId, ClassId, ptrans.No_of_Worksheetstobeassigned, Convert.ToInt32(ptrans.SubjectId));
            if (status == true)
            {
                ptrans.Isworksheetsassigned = true;
            }
            else
            {
                ptrans.Isworksheetsassigned = false;
            }
            Update(ptrans);
            using (LogiqguruEntities context = new LogiqguruEntities())
            {
                Tbl_Subjects_Info subdata = context.Tbl_Subjects_Info.Where(x => x.SubjectName == "curriculum" && x.ClassId == ClassId).FirstOrDefault();
                return (context.Tbl_Chapter_Info.Where(x => x.ClassId == ClassId && x.SubjectId == subdata.SubjectId).Count()) * 3;
            }
        }

    }
    public class Awardscurriculumrepository : RepositoryBase<Tbl_Curruculumcoinshistory>
    {
        public List<Tbl_Curruculumcoinshistory> Getcoinshistory(long SubUserId)
        {
            return Get().Where(x => x.SubuserId == SubUserId && x.IsCoin == true).OrderByDescending(x => x.CoinId).ToList();
        }
        public List<Tbl_Curruculumcoinshistory> Getdiamondshistory(long SubUserId)
        {
            return Get().Where(x => x.SubuserId == SubUserId && x.IsCoin == false).OrderByDescending(x => x.CoinId).ToList();
        }
        //public bool InsertCoinshistory(string TotalCoins, long SubUserId)
        //{

        //}
    }
    public class Awardsrepository : RepositoryBase<Tbl_Coinshistory>
    {
        public List<Tbl_Coinshistory> Getcoinshistory(long SubUserId)
        {
            return Get().Where(x => x.SubuserId == SubUserId && x.IsCoin == true).OrderByDescending(x => x.CoinId).ToList();
        }
    }
    public class UpdateAwardsrepository : RepositoryBase<Tbl_Availablecoins>
    {
        public bool Update(int worksheetresult, long SubUserId)
        {
            Tbl_Availablecoins currentcoins = Get().Where(x => x.SubUserId == SubUserId).FirstOrDefault();
            currentcoins.Totalcoins = (Convert.ToInt32(currentcoins.Totalcoins) + (worksheetresult * 20)).ToString();
            currentcoins.Totalcoinsearned = (Convert.ToInt32(currentcoins.Totalcoinsearned) + (worksheetresult * 20)).ToString();
            Tbl_Coinshistory coins = new Tbl_Coinshistory();

            coins.SubuserId = SubUserId;
            coins.CreatedOn = DateTime.Now;
            if (worksheetresult == 10)
            {
                coins.TotalCoins = ((worksheetresult * 20) + 50).ToString();

                coins.Credit = ((worksheetresult * 20) + 50).ToString();
                coins.Messages = ((worksheetresult * 20) + 50) + "Credited successfully";

            }
            else
            {
                coins.TotalCoins = (worksheetresult * 20).ToString();

                coins.Credit = (worksheetresult * 20).ToString();
                coins.Messages = worksheetresult * 20 + "Credited successfully";

            }

            if (currentcoins.Totalcoins == "1000")
            {
                currentcoins.Totalcoins = (Convert.ToInt32(currentcoins.Totalcoins) - 1000).ToString();
                currentcoins.TotalDiamonds = (Convert.ToInt32(currentcoins.TotalDiamonds) + 1).ToString();
                coins.Debit = "1000";
                coins.Messages = coins.Messages + ("1000 coins redeemed for 1 Diamond");
            }
            coins.balance = (Convert.ToInt32(currentcoins.Totalcoins) + Convert.ToInt32(coins.Credit)).ToString();


            coins.IsCoin = true;
            Update(currentcoins);

            Awardsrepository arep = new Awardsrepository();
            arep.Insert(coins);
            //arep.InsertCoinshistory(currentcoins.Totalcoins,SubUserId,);
            return true;
        }
        public bool Insertfirstrecord(long SubuserId)
        {
            Tbl_Availablecoins ac = new Tbl_Availablecoins();
            ac.SubUserId = SubuserId;
            ac.Totalcoins = "0";
            ac.TotalDiamonds = "0";
            Insert(ac);
            return true;
        }
        public bool Updatewithonediamondwhenstreakisthree(long SubUserId)
        {
            StreakRepository srep = new StreakRepository();
            int streakcount = srep.Streakcount(SubUserId);
            if (streakcount >= 3)
            {
                Tbl_Availablecoins coins = Get().Where(x => x.SubUserId == SubUserId).FirstOrDefault();
                coins.TotalDiamonds = (Convert.ToInt32(coins.TotalDiamonds) + 1).ToString();
                Update(coins);
            }
            return true;
        }
    }
    public class UpdateAwardscurriculumrepository : RepositoryBase<Tbl_Curruculumavailablecoins>
    {
        public bool Update(int worksheetresult, long SubUserId, Tbl_Curriculamworksheets worksheetdata)
        {
            using (LogiqguruEntities context = new LogiqguruEntities())
            {
                Tbl_Chapter_Info chapdata = context.Tbl_Chapter_Info.Where(x => x.ChapterId == worksheetdata.ChapterId).FirstOrDefault();
                Tbl_Curruculumavailablecoins currentcoins = Get().Where(x => x.SubUserId == SubUserId).FirstOrDefault();
                Tbl_Curruculumcoinshistory coins = new Tbl_Curruculumcoinshistory();

                coins.SubuserId = SubUserId;
                coins.CreatedOn = DateTime.Now;
                if (worksheetresult == 10)
                {
                    //coins.TotalCoins = ((worksheetresult * 20) + 50).ToString();
                    coins.Credit = ((worksheetresult * 20) + 50).ToString();
                    coins.Messages = ((worksheetresult * 20) + 50) + " Coins Credited successfully for chapter " + chapdata.ChapterName + " and Drill " + worksheetdata.Examtype;
                }
                else
                {
                    //coins.TotalCoins = (worksheetresult * 20).ToString();
                    coins.Credit = (worksheetresult * 20).ToString();
                    coins.Messages = worksheetresult * 20 + " Coins Credited successfully for chapter " + chapdata.ChapterName + " and Drill " + worksheetdata.Examtype;
                }


                currentcoins.Totalcoinsearned = (Convert.ToInt32(currentcoins.Totalcoinsearned) + Convert.ToInt32(coins.Credit)).ToString();
                currentcoins.Totalcoins = (Convert.ToInt32(currentcoins.Totalcoins) + Convert.ToInt32(coins.Credit)).ToString();
                coins.TotalCoins = currentcoins.Totalcoinsearned;
                coins.IsCoin = true;



                coins.balance = (Convert.ToInt32(currentcoins.Totalcoins)).ToString();

                Update(currentcoins);

                Awardscurriculumrepository arep = new Awardscurriculumrepository();
                arep.Insert(coins);
                if (Convert.ToInt32(currentcoins.Totalcoins) >= 1000)
                {
                    currentcoins.Totalcoins = (Convert.ToInt32(currentcoins.Totalcoins) - 1000).ToString();
                    currentcoins.TotalDiamonds = (Convert.ToInt32(currentcoins.TotalDiamonds) + 1).ToString();
                    coins.Debit = "1000";
                    coins.Credit = "";
                    coins.Messages = ("1000 coins redeemed for 1 Diamond");
                    coins.balance = (Convert.ToInt32(currentcoins.Totalcoins)).ToString();
                    Update(currentcoins);
                    arep.Insert(coins);
                    coins.Credit = "1";
                    coins.Debit = "";

                    coins.TotalCoins = "0";
                    coins.IsCoin = false;
                    coins.Messages = ("1 diamond credited");
                    coins.balance = (Convert.ToInt32(currentcoins.TotalDiamonds)).ToString();
                    arep.Insert(coins);

                }
            }
            //arep.InsertCoinshistory(currentcoins.Totalcoins,SubUserId,);
            return true;
        }
        public bool Insertfirstrecord(long SubuserId)
        {
            Tbl_Curruculumavailablecoins ac = new Tbl_Curruculumavailablecoins();
            ac.SubUserId = SubuserId;
            ac.Totalcoins = "0";
            ac.TotalDiamonds = "0";
            Insert(ac);
            return true;
        }
        public bool Updatewithonediamondwhenstreakisthree(long SubUserId)
        {
            StreakRepository srep = new StreakRepository();
            int streakcount = srep.Streakcount(SubUserId);
            if (streakcount >= 3)
            {
                Tbl_Curruculumavailablecoins coins = Get().Where(x => x.SubUserId == SubUserId).FirstOrDefault();
                coins.TotalDiamonds = (Convert.ToInt32(coins.TotalDiamonds) + 1).ToString();
                Update(coins);
            }
            return true;
        }
    }

    public class StreakRepository : RepositoryBase<Tbl_DailyGoal>
    {
        public int Streakcount(long SubUserId)
        {
            using (LogiqguruEntities context = new LogiqguruEntities())
            {
                return context.Database.SqlQuery<int>("SELECT COUNT(DISTINCT AchievedDate) AS days FROM   Tbl_DailyGoal  where SubUserId = " + SubUserId + " and AchievedDate Between GETDATE() - 3 and GETDATE()").FirstOrDefault();
            }
        }
        public bool Checkandupdatedailygoal(long SubUserId)
        {
            if (Get().Where(x => x.AchievedDate == DateTime.Today && x.SubUserId == SubUserId).FirstOrDefault() == null)
            {
                Tbl_DailyGoal insertdata = new Tbl_DailyGoal();
                insertdata.AchievedDate = DateTime.Now;
                insertdata.SubUserId = SubUserId;
                Insert(insertdata);
                UpdateAwardsrepository urep = new UpdateAwardsrepository();
                urep.Updatewithonediamondwhenstreakisthree(SubUserId);
            }
            return true;
        }
    }
    public class Videorepository : RepositoryBase<Tbl_Content>
    {
        public List<Tbl_Content> Getvideos(long ChapterId, long Classid)
        {
            return Get().Where(x => x.Subj_Chap_id == ChapterId && x.ClassId == Classid).ToList();
        }
        public List<Chapterdata> Getallchaptersforclass(long Classid)
        {
            using (LogiqguruEntities context = new LogiqguruEntities())
            {
                List<Chapterdata> chapterdata = new List<Chapterdata>();
                List<int> chapterid = context.Database.SqlQuery<int>("select c.ChapterId as chapterid from Tbl_Chapter_Info as  c join[dbo].[Tbl_Subjects_Info] as s on s.SubjectId = c.SubjectId where c.ClassId = " + Classid + " and s.SubjectName != 'Pre-Test'").ToList();
                foreach (var item in chapterid)
                {
                    Tbl_Chapter_Info chapterdatafromdb = context.Tbl_Chapter_Info.Where(x => x.ChapterId == item).FirstOrDefault();
                    Chapterdata cdata = new Chapterdata();
                    cdata.ChapterId = chapterdatafromdb.ChapterId;
                    cdata.Chaptername = chapterdatafromdb.ChapterName;
                    chapterdata.Add(cdata);
                }
                return chapterdata;
            }
        }
    }
    public class CurriculamQuestionsRepository : RepositoryBase<TBl_CurriculamQuestions>
    {
        public List<Questionswithpre> Getquestions(long SubUserId)
        {
            //List<TBl_Questions> questions = new List<TBl_Questions>();
            using (LogiqguruEntities context = new LogiqguruEntities())
            {
                Tbl_Curriculampretest pretest = context.Tbl_Curriculampretest.Where(x => x.SubUserId == SubUserId).FirstOrDefault();
                if (pretest == null)
                {
                    Tbl_Subusers subuserdata = context.Tbl_Subusers.Where(x => x.SubUserId == SubUserId).FirstOrDefault();
                    CurriculampretestRepository rep = new CurriculampretestRepository();
                    rep.Insertpretestdata(SubUserId, subuserdata.ClassId);
                }
                List<Questionswithpre> questions = context.Database.SqlQuery<Questionswithpre>("SELECT *  FROM [dbo].[Tbl_Curriculampretest] as pre join TBl_CurriculamQuestions as q on q.Q_Id=pre.QuestionId where pre.SubUserId=" + SubUserId).ToList();
                //context.Tbl_Pretest.Where(x =>x.SubUserId==SubUserId ).ToList().Select(r => r.QuestionId).ToArray();
                //foreach(var item in Qids)
                //{
                //    questions.Add(Get(x => x.Q_Id == item).FirstOrDefault());
                //}
                return questions;

            }
        }
    }
    public class CurriculampretestRepository : RepositoryBase<Tbl_Curriculampretest>
    {
        public List<Questionswithpre> Submitpretest(long Subuserid)
        {
            using (LogiqguruEntities context = new LogiqguruEntities())
            {

                return context.Database.SqlQuery<Questionswithpre>("SELECT *  FROM [dbo].[Tbl_Curriculampretest] as pre join TBl_CurriculamQuestions as q on q.Q_Id=pre.QuestionId where pre.SubUserId=" + Subuserid).ToList();
            }
        }
        public bool Updatepresetwhenanswered(long Pretestid, bool Answer, string selectedanswer, int totalquestions)
        {
            using (LogiqguruEntities context = new LogiqguruEntities())
            {
                Tbl_Curriculampretest datatoupdate = context.Tbl_Curriculampretest.Where(x => x.PreTestID == Pretestid).FirstOrDefault();
                datatoupdate.IsAnswered = true;
                datatoupdate.IsCorrect = Answer;
                datatoupdate.Answered = selectedanswer;
                Update(datatoupdate);

                if (totalquestions == context.Tbl_Curriculampretest.Where(x => x.SubUserId == datatoupdate.SubUserId && x.IsAnswered == true).Count())
                {
                    int pretestresult = context.Tbl_Curriculampretest.Where(x => x.SubUserId == datatoupdate.SubUserId && x.IsCorrect == true).Count();

                    Tbl_Subusers updateresult = context.Tbl_Subusers.Where(x => x.SubUserId == datatoupdate.SubUserId).FirstOrDefault();
                    updateresult.Marksgainedincurriculumpretest = pretestresult;
                    context.SaveChanges();

                    //Worksheetrepository rep = new Worksheetrepository();
                    //List<Tbl_Paymenttransactions> ptrans = context.Tbl_Paymenttransactions.Where(x => x.Isworksheetsassigned == false && x.SubUserId == updateresult.SubUserId).ToList();
                    //if (ptrans != null)
                    //{
                    //    rep.Insertpaidworksheets(datatoupdate.SubUserId, updateresult.ClassId, ptrans.Sum(x => x.No_of_Worksheetstobeassigned), Convert.ToInt32(ptrans.FirstOrDefault().SubjectId));
                    //    foreach (var item in ptrans)
                    //    {
                    //        item.Isworksheetsassigned = true;
                    //        context.SaveChanges();

                    //    }
                    //}
                    Badgesrepository brep = new Badgesrepository();
                    brep.Badgeafterpretes(datatoupdate.SubUserId);


                }
            }
            return true;
        }
        public bool Insertpretestdata(long SubUserid, int ClassId)
        {
            List<long> Qids = new List<long>();
            LogiqguruEntities context = new LogiqguruEntities();
            Tbl_Subjects_Info subinfo = context.Tbl_Subjects_Info.Where(x => x.SubjectName == "Curriculum Pre-Test" && x.ClassId == ClassId).FirstOrDefault();
            Qids = Getquestionidstoinsert(ClassId, subinfo, SubUserid);
            foreach (var item in Qids)
            {
                Tbl_Curriculampretest data = new Tbl_Curriculampretest();
                data.QuestionId = item;
                data.SubUserId = SubUserid;
                Insert(data);
            }
            return true;
        }
        public List<long> Getquestionidstoinsert(int ClassId, Tbl_Subjects_Info subinfo, long Userid)
        {
            LogiqguruEntities context = new LogiqguruEntities();
            List<long> Qids = new List<long>();
            long[] easyquestionsidentificationchap1 = context.TBl_CurriculamQuestions.Where(x => x.ClassId == ClassId && x.SubjectId == subinfo.SubjectId).ToList().Select(r => r.Q_Id).ToArray();

            long[] shuffledeasyids = Shuffle<long>(easyquestionsidentificationchap1);

            for (int i = 0; i <= (int)Custom.pretest.total; i++)
            {

                foreach (var item in shuffledeasyids.Take(15))
                {
                    if (context.Tbl_Pretest.Where(x => x.QuestionId == item && x.SubUserId == Userid).FirstOrDefault() == null)
                    {
                        if (!Qids.Contains(item))
                        {
                            Qids.Add(item);
                            break;
                        }
                    }
                }
            }
            return Qids;
        }
        public T[] Shuffle<T>(T[] array)
        {
            Random _random = new Random();
            var random = _random;
            for (int i = array.Length; i > 1; i--)
            {
                // Pick random element to swap.
                int j = random.Next(i); // 0 <= j <= i-1
                                        // Swap.
                T tmp = array[j];
                array[j] = array[i - 1];
                array[i - 1] = tmp;
            }
            return array;
        }
    }
    public class CurriculammainRepository : RepositoryBase<Tbl_Curriculamworksheets>
    {
        public List<Worksheethistory> Getcurriculumworksheethistory(long SubUserid)
        {
            using (LogiqguruEntities context = new LogiqguruEntities())
            {
                return context.Database.SqlQuery<Worksheethistory>("select cq.[Curriculamid] as WorksheetId, max(c.ChapterName) + ' Drill' + CAST(cw.Examtype  AS NVARCHAR(10)) as ChapterName , max(cw.Completeddate) as Completeddate, COUNT(cq.IsCorrect) as Marksgainedinthissheet, (COUNT(cq.IsCorrect) * 10) as Accuracy, (COUNT(cq.IsCorrect) * 20) as Coins   FROM[db_Logiqguru].[dbo].[Tbl_Curriculamexamquestions] cq   join[dbo].[Tbl_Curriculamworksheets]  cw  on cq.Curriculamid = cw.Curriculamid   and cw.IsCompleted=1  join[dbo].Tbl_Chapter_Info c on c.ChapterId = cw.ChapterId    where cq.IsCorrect = 1 and cw.subuserid = " + SubUserid + " group by cq.[Curriculamid], cw.Examtype").ToList();
            }

        }
        public List<Chapterdata> Getcurriculamchapters(long SubUserid)
        {

            using (LogiqguruEntities context = new LogiqguruEntities())
            {
                List<Chapterdata> chapterdata = new List<Chapterdata>();
                List<long> chapterid = context.Database.SqlQuery<long>("SELECT [ChapterId]   FROM [dbo].[Tbl_Curriculamworksheets] where SubUserId=" + SubUserid + " ").ToList();
                foreach (var item in chapterid)
                {
                    Tbl_Chapter_Info chapterdatafromdb = context.Tbl_Chapter_Info.Where(x => x.ChapterId == item).FirstOrDefault();
                    Chapterdata cdata = new Chapterdata();
                    cdata.ChapterId = chapterdatafromdb.ChapterId;
                    cdata.Chaptername = chapterdatafromdb.ChapterName;
                    cdata.IsAssessment = chapterdatafromdb.IsAssessment;
                    chapterdata.Add(cdata);
                }
                return chapterdata;
            }

        }
        public List<Curirriculumwithlinks> Getcurriculamdrillsload(long SubUserid)
        {
            //List<Tbl_Curriculamworksheets> data = Get().Where(x => x.SubUserId == SubUserid && x.ChapterId == ChapterId).ToList();
            using (LogiqguruEntities context = new LogiqguruEntities())
            {
                Tbl_Curriculamworksheets CURDATA = Getcurriculumlastidvideo(SubUserid);
                Tbl_Curriculamworksheets datatoupdate = context.Tbl_Curriculamworksheets.Where(x => x.SubUserId == SubUserid && x.ChapterId == CURDATA.ChapterId && x.Examtype == 1 && x.Tobeopened == true).FirstOrDefault();
                if (datatoupdate != null)
                {
                    if (datatoupdate.IsVideoopened != true)
                    {
                        datatoupdate.IsVideoopened = true;
                        datatoupdate.Accessgranteddate = DateTime.Now.AddDays(Convert.ToDouble(WebConfigurationManager.AppSettings["Curriculumtime"]));
                        context.SaveChanges();
                    }
                }
                List<Curirriculumwithlinks> data = context.Database.SqlQuery<Curirriculumwithlinks>("select cw.*,c.Url as Videolink,cp.Url as Pdflink from Tbl_Curriculamworksheets as cw left join [Tbl_Content] as c on cw.ChapterId =c.Subj_Chap_id and c.Filetype='Video' left join [Tbl_Content] as cp on cw.ChapterId = cp.Subj_Chap_id and cp.Filetype = 'Content' where SubUserid=" + SubUserid + " and ChapterId=" + CURDATA.ChapterId).ToList();
                return data;
            }
        }
        public List<Curirriculumwithlinks> Getcurriculamdrills(long ChapterId, long SubUserid)
        {
            //List<Tbl_Curriculamworksheets> data = Get().Where(x => x.SubUserId == SubUserid && x.ChapterId == ChapterId).ToList();
            using (LogiqguruEntities context = new LogiqguruEntities())
            {
                Tbl_Curriculamworksheets datatoupdate = context.Tbl_Curriculamworksheets.Where(x => x.SubUserId == SubUserid && x.ChapterId == ChapterId && x.Examtype == 1 && x.Tobeopened == true).FirstOrDefault();
                if (datatoupdate != null)
                {
                    if (datatoupdate.IsVideoopened != true)
                    {
                        datatoupdate.IsVideoopened = true;
                        datatoupdate.Accessgranteddate = DateTime.Now.AddDays(Convert.ToDouble(WebConfigurationManager.AppSettings["Curriculumtime"]));
                        context.SaveChanges();
                    }
                }
                List<Curirriculumwithlinks> data = context.Database.SqlQuery<Curirriculumwithlinks>("select cw.*,c.Url as Videolink,c.Filename as Videoname,cp.Filename as Pdfname,cp.Url as Pdflink from Tbl_Curriculamworksheets as cw left join [Tbl_Content] as c on cw.ChapterId =c.Subj_Chap_id and c.Filetype='Video' left join [Tbl_Content] as cp on cw.ChapterId = cp.Subj_Chap_id and cp.Filetype = 'Content' where SubUserid=" + SubUserid + " and ChapterId=" + ChapterId).ToList();
                return data;
            }
        }
        public List<Curirriculumwithlinks> Curriculumassesmentdrills(long ChapterId, long SubUserid)
        {
            //List<Tbl_Curriculamworksheets> data = Get().Where(x => x.SubUserId == SubUserid && x.ChapterId == ChapterId).ToList();
            using (LogiqguruEntities context = new LogiqguruEntities())
            {
                Tbl_Curriculamworksheets datatoupdate = context.Tbl_Curriculamworksheets.Where(x => x.SubUserId == SubUserid && x.ChapterId == ChapterId && x.Examtype == 4 && x.Tobeopened == true).FirstOrDefault();
                if (datatoupdate != null)
                {
                    if (datatoupdate.IsVideoopened != true)
                    {
                        datatoupdate.IsVideoopened = true;
                        datatoupdate.Accessgranteddate = DateTime.Now.AddDays(Convert.ToDouble(WebConfigurationManager.AppSettings["Curriculumtime"]));
                        context.SaveChanges();
                    }
                }
                List<Curirriculumwithlinks> data = context.Database.SqlQuery<Curirriculumwithlinks>("select cw.*,c.Url as Videolink,c.Filename as Videoname,cp.Filename as Pdfname,cp.Url as Pdflink from Tbl_Curriculamworksheets as cw left join [Tbl_Content] as c on cw.ChapterId =c.Subj_Chap_id and c.Filetype='Video' left join [Tbl_Content] as cp on cw.ChapterId = cp.Subj_Chap_id and cp.Filetype = 'Content' where SubUserid=" + SubUserid + " and ChapterId=" + ChapterId).ToList();
                return data;
            }
        }

        public List<Questionscurriculum> Getquestionsofworksheet(long Curriculamid)
        {
            using (LogiqguruEntities context = new LogiqguruEntities())
            {
                //Tbl_Worksheets updateresult = context.Tbl_Worksheets.Where(x => x.WorksheetId == Worksheetid).FirstOrDefault();
                //updateresult.Status = (int)Custom.Worksheetstatus.Opened;
                //context.SaveChanges();
                return context.Database.SqlQuery<Questionscurriculum>("SELECT q.Objective,s.SubTopicName, ql.Text,wq.*,q.* FROM [dbo].Tbl_Curriculamexamquestions as wq  join dbo.TBl_CurriculamQuestions as q on q.q_id = wq.q_id left join dbo.Deff_QueationLevel as ql on ql.QLevelID = q.QLevelID left join dbo.Tbl_SubTopics_info as s on s.SubTopicID = q.SubTopicId where Curriculamid=" + Curriculamid).ToList();
            }
        }

        public bool Insertworksheetquestions(long Curriculamid, int Examtype, long Subuserid)
        {
            LogiqguruEntities context = new LogiqguruEntities();
            long[] questions = getquestionsbasedonlevel(Curriculamid, Examtype, Subuserid);
            Pretestrepository rep = new Pretestrepository();
            List<long> Qids = new List<long>();
            long[] shuffledeasyids = rep.Shuffle<long>(questions);
            foreach (var item in shuffledeasyids.Take(10))
            {
                if (context.Tbl_Curriculamexamquestions.Where(x => x.Q_Id == item && x.SubUserId == Subuserid).FirstOrDefault() == null)
                {
                    if (!Qids.Contains(item))
                    {
                        Qids.Add(item);
                    }
                }
            }
            int? marksgained = 0;
            marksgained = context.Tbl_Subusers.Where(x => x.SubUserId == Subuserid).FirstOrDefault().Marksgainedincurriculumpretest;
            if (marksgained != null)
            {
                if (Qids.Count() == 10)
                {
                    foreach (var item in Qids)
                    {
                        Tbl_Curriculamexamquestions data = new Tbl_Curriculamexamquestions();

                        data.Q_Id = item;
                        data.SubUserId = Subuserid;
                        data.CreatedOn = DateTime.Now;
                        data.Curriculamid = Curriculamid;
                        context.Tbl_Curriculamexamquestions.Add(data);
                    }
                    context.SaveChanges();

                }
            }
            return true;
        }
        public long[] getquestionsbasedonlevel(long Curriculamid, int Examtype, long Subuserid)
        {
            using (LogiqguruEntities context = new LogiqguruEntities())
            {
                Tbl_Curriculamworksheets cdata = context.Tbl_Curriculamworksheets.Where(x => x.Curriculamid == Curriculamid).FirstOrDefault();
                Tbl_Subusers data = context.Tbl_Subusers.Where(x => x.SubUserId == Subuserid).FirstOrDefault();
                //Tbl_Curriculamworksheets currentworsheetdata = context.Tbl_Curriculamworksheets.Where(x => x.Curriculamid == Curriculamid && x.SubUserId == Subuserid).FirstOrDefault();
                if (Examtype != 4)
                    return context.TBl_CurriculamQuestions.Where(x => x.ClassId == data.ClassId && x.QLevelID == Examtype && x.Qstatus == (int)Custom.Status.Approved && x.ChapterId == cdata.ChapterId && x.Isactive == true).ToList().Select(r => r.Q_Id).ToArray();
                else
                    return context.TBl_CurriculamQuestions.Where(x => x.ClassId == data.ClassId && x.Qstatus == (int)Custom.Status.Approved && x.ChapterId == cdata.ChapterId && x.Isactive == true).ToList().Select(r => r.Q_Id).ToArray();

            }
        }

        public bool Updatewronganswercurriculum(long Cqid, bool Answer, string selectedanswer, int totalquestions)
        {
            using (LogiqguruEntities context = new LogiqguruEntities())
            {
                Tbl_Curriculamexamquestions datatoupdate = context.Tbl_Curriculamexamquestions.Where(x => x.CQId == Cqid).FirstOrDefault();

                datatoupdate.Wronganswergivenbyuser = selectedanswer;
                datatoupdate.Attemptcount = 1;
                context.SaveChanges();
                return true;
            }
        }
        public bool Updatecurriculumwhenanswered(long Cqid, bool Answer, string selectedanswer, int totalquestions)
        {
            using (LogiqguruEntities context = new LogiqguruEntities())
            {
                Tbl_Curriculamexamquestions datatoupdate = context.Tbl_Curriculamexamquestions.Where(x => x.CQId == Cqid).FirstOrDefault();
                datatoupdate.IsAnswered = true;
                datatoupdate.IsCorrect = Answer;
                datatoupdate.Answergivenbyuser = selectedanswer;
                datatoupdate.AnsweredOn = DateTime.Now;
                context.SaveChanges();

                if (totalquestions == context.Tbl_Curriculamexamquestions.Where(x => x.Curriculamid == datatoupdate.Curriculamid && x.IsAnswered == true).Count())
                {
                    int worksheetresult = context.Tbl_Curriculamexamquestions.Where(x => x.Curriculamid == datatoupdate.Curriculamid && x.IsCorrect == true).Count();
                    Tbl_Curriculamworksheets updateresult = Get().Where(x => x.Curriculamid == datatoupdate.Curriculamid).FirstOrDefault();
                    updateresult.Status = (int)Custom.Worksheetstatus.Completed;
                    updateresult.IsCompleted = true;
                    updateresult.Completeddate = DateTime.Now;
                    Update(updateresult);
                    Tbl_Curriculamworksheets nextworksheettobeopened = Get().Where(x => x.IsCompleted == null && x.SubUserId == updateresult.SubUserId).FirstOrDefault();
                    nextworksheettobeopened.Tobeopened = true;
                    nextworksheettobeopened.Accessgranteddate = DateTime.Now.AddDays(Convert.ToDouble(WebConfigurationManager.AppSettings["Curriculumtime"]));

                    Update(nextworksheettobeopened);
                    UpdateAwardscurriculumrepository uarep = new UpdateAwardscurriculumrepository();
                    uarep.Update(worksheetresult, datatoupdate.SubUserId.Value, updateresult);
                    Badgesrepository brep = new Badgesrepository();
                    brep.Checkbadges(Convert.ToInt32(datatoupdate.SubUserId));
                    //if (worksheetresult >= 5)
                    //{
                    //    StreakRepository srep = new StreakRepository();
                    //    srep.Checkandupdatedailygoal(datatoupdate.SubUserId.Value);
                    //}
                }
            }
            return true;
        }
        public List<Questionscurriculum> Submitcurriculum(long Cqid, long SubUserId, int chapterid)
        {
            using (LogiqguruEntities context = new LogiqguruEntities())
            {
                //int worksheetresult = context.Tbl_Worksheetquestions.Where(x => x.WorksheetId == Worksheetid && x.IsCorrect == true).Count();
                //Tbl_Worksheets updateresult = context.Tbl_Worksheets.Where(x => x.WorksheetId == Worksheetid).FirstOrDefault();
                //updateresult.MarksGained = worksheetresult;
                //updateresult.Status = (int)Custom.Worksheetstatus.Completed;
                //updateresult.IsCompleted = true;
                //context.SaveChanges();
                //Tbl_Worksheets nextworksheettobeopened = context.Tbl_Worksheets.Where(x => x.Status == (int)Custom.Worksheetstatus.Created && x.IsCompleted == null && x.SubUserId == SubUserId && x.ChapterId == chapterid).FirstOrDefault();
                //nextworksheettobeopened.Tobeopened = true;
                //context.SaveChanges();

                return context.Database.SqlQuery<Questionscurriculum>("SELECT q.Objective,s.SubTopicName, ql.Text,wq.*,q.*,(select  (Cast (A AS decimal(10,2)) / Cast (B AS decimal(10,2))*100) from ( select count(*) as A from dbo.Tbl_Curriculamexamquestions where Q_Id = q.q_id and IsCorrect = 1) X join (select count(*) as B from dbo.Tbl_Curriculamexamquestions where Q_Id = q.q_id) Y on 1 = 1)  as TotalValue FROM [dbo].Tbl_Curriculamexamquestions as wq  join dbo.TBl_CurriculamQuestions as q on q.q_id = wq.q_id left join dbo.Deff_QueationLevel as ql on ql.QLevelID = q.QLevelID left join dbo.Tbl_SubTopics_info as s on s.SubTopicID = q.SubTopicId where Curriculamid=" + Cqid).ToList();
            }
        }
        public CurriculumDashboard Getdashboarddata(long subuserid)
        {
            CurriculumDashboard data = new CurriculumDashboard();
            data.Totalworksheets = Get().Where(x => x.SubUserId == subuserid).Count();
            data.Pendingworksheets = Get().Where(x => x.SubUserId == subuserid && x.IsCompleted == null).Count();
            data.Completedworksheets = Get().Where(x => x.SubUserId == subuserid && x.IsCompleted == true).Count();
            //decimal? Totalmarksgained = Get().Where(x => x.IsCompleted == true && x.SubUserId == subuserid).Sum(x => x.MarksGained);
            //decimal Totalmarks = data.Completedworksheets * 10;
            //if (Totalmarksgained.Value != 0)
            //{
            //    data.Totalprofiency = (int)((Totalmarksgained.Value / Totalmarks) * 100);
            //}
            data.Worksheets = Get().Where(x => x.IsCompleted == true && x.SubUserId == subuserid).ToList();
            using (LogiqguruEntities context = new LogiqguruEntities())
            {
                Tbl_Curruculumavailablecoins cdata = context.Tbl_Curruculumavailablecoins.Where(x => x.SubUserId == subuserid).FirstOrDefault();
                if (cdata != null)
                {
                    data.Totalcoins = cdata.Totalcoins;
                    data.Totaldiamonds = cdata.TotalDiamonds;
                }
                else
                {
                    data.Totalcoins = "0";
                    data.Totaldiamonds = "0";

                }
                decimal correct = context.Tbl_Curriculamexamquestions.Where(x => x.SubUserId == subuserid && x.IsCorrect == true).Count();
                decimal totalanswered = context.Tbl_Curriculamexamquestions.Where(x => x.SubUserId == subuserid && x.IsAnswered == true).Count();
                decimal totaldivision = 0;
                if (totalanswered != 0)
                {
                    totaldivision = (correct / totalanswered);
                }
                data.Totalprofiency = (totaldivision * 100);
                data.Totalbadgesgained = context.Tbl_Badges.Where(x => x.SubUserId == subuserid).Count();
            }
            return data;
        }
        public Tbl_Curriculamworksheets Getcurriculumlastid(long Subuserid)
        {
            using (LogiqguruEntities context = new LogiqguruEntities())
            {
                Tbl_Curriculamworksheets data = new Tbl_Curriculamworksheets();
                data = context.Tbl_Curriculamworksheets.Where(x => x.SubUserId == Subuserid && x.Tobeopened == true && x.Accessgranteddate <= DateTime.Now).OrderByDescending(x => x.Curriculamid).FirstOrDefault();
                if (data != null)
                {
                    return data;
                }
                else
                {
                    return null;
                }
            }
        }
        public Tbl_Curriculamworksheets Getcurriculumlastidvideo(long Subuserid)
        {
            using (LogiqguruEntities context = new LogiqguruEntities())
            {
                Tbl_Curriculamworksheets data = new Tbl_Curriculamworksheets();
                data = context.Tbl_Curriculamworksheets.Where(x => x.SubUserId == Subuserid && x.Tobeopened == true).OrderByDescending(x => x.Curriculamid).FirstOrDefault();
                if (data != null)
                {
                    return data;
                }
                else
                {
                    return null;
                }
            }
        }

    }
    public class Badgesrepository : RepositoryBase<Tbl_Badges>
    {
        public void Badgeafterpretes(long Subuserid)
        {
            using (LogiqguruEntities context = new LogiqguruEntities())
            {
                List<Deff_Badges> deff_Badges = context.Deff_Badges.ToList();
                Tbl_Badges data = new Tbl_Badges();
                data.BadgeId = (int)Custom.BadegIds.Upandloaded;
                data.SubUserId = Subuserid;
                data.CreatedOn = DateTime.Now;
                Insert(data);
                Tbl_Curruculumavailablecoins coinsdata = context.Tbl_Curruculumavailablecoins.Where(x => x.SubUserId == Subuserid).FirstOrDefault();
                coinsdata.TotalDiamonds = coinsdata.TotalDiamonds + deff_Badges.Where(x => x.BadgeId == (int)Custom.BadegIds.Upandloaded).FirstOrDefault().Diamondstobeawarded;
                context.SaveChanges();
                Tbl_Curruculumcoinshistory tbl_Coinshistory = new Tbl_Curruculumcoinshistory();
                tbl_Coinshistory.Credit = deff_Badges.Where(x => x.BadgeId == (int)Custom.BadegIds.Upandloaded).FirstOrDefault().Diamondstobeawarded.ToString();
                tbl_Coinshistory.Debit = "0";
                tbl_Coinshistory.SubuserId = Subuserid;
                tbl_Coinshistory.CreatedOn = DateTime.Now;
                tbl_Coinshistory.balance = coinsdata.TotalDiamonds;
                tbl_Coinshistory.TotalCoins = "";
                tbl_Coinshistory.Messages = "1 diamond credited for Upandloaded";
                tbl_Coinshistory.IsCoin = false;
                context.Tbl_Curruculumcoinshistory.Add(tbl_Coinshistory);
                context.SaveChanges();
            }
        }
        public List<Badges> Getcurruculumbadge(long Subuserid)
        {
            using (LogiqguruEntities context = new LogiqguruEntities())
            {
                return context.Database.SqlQuery<Badges>("select Ba.[BadgeId], Ba.[SubUserId],Ba.CreatedOn,db.BadgeName,db.BadgeDescription,db.Diamondstobeawarded,db.Image from Tbl_Badges Ba join Deff_Badges db on db.BadgeId = ba.BadgeId where ba.SubUserId = " + Subuserid).ToList();
            }
        }
        public void Checkbadges(long Subuserid)
        {
            LogiqguruEntities context = new LogiqguruEntities();
            List<Tbl_Curriculamworksheets> toalcurriculumdata = context.Tbl_Curriculamworksheets.Where(x => x.SubUserId == Subuserid).ToList();
            List<Tbl_Badges> totalbadgedata = Get().Where(x => x.SubUserId == Subuserid).ToList();
            List<Deff_Badges> deff_Badges = context.Deff_Badges.ToList();
            if (toalcurriculumdata.FirstOrDefault().IsCompleted == true && totalbadgedata.Where(x => x.BadgeId == (int)Custom.BadegIds.trigger).FirstOrDefault() != null)
            {
                Tbl_Badges data = new Tbl_Badges();
                data.BadgeId = (int)Custom.BadegIds.trigger;
                data.SubUserId = Subuserid;
                data.CreatedOn = DateTime.Now;
                Insert(data);
                Tbl_Curruculumavailablecoins coinsdata = context.Tbl_Curruculumavailablecoins.Where(x => x.SubUserId == Subuserid).FirstOrDefault();
                coinsdata.TotalDiamonds = coinsdata.TotalDiamonds + deff_Badges.Where(x => x.BadgeId == (int)Custom.BadegIds.trigger).FirstOrDefault().Diamondstobeawarded;
                context.SaveChanges();
                Tbl_Curruculumcoinshistory tbl_Coinshistory = new Tbl_Curruculumcoinshistory();
                tbl_Coinshistory.Credit = deff_Badges.Where(x => x.BadgeId == (int)Custom.BadegIds.trigger).FirstOrDefault().Diamondstobeawarded.ToString();
                tbl_Coinshistory.Debit = "0";
                tbl_Coinshistory.SubuserId = Subuserid;
                tbl_Coinshistory.CreatedOn = DateTime.Now;
                tbl_Coinshistory.balance = coinsdata.TotalDiamonds;
                tbl_Coinshistory.TotalCoins = "";
                tbl_Coinshistory.Messages = "1 diamond credited for trigger";
                tbl_Coinshistory.IsCoin = false;
                context.Tbl_Curruculumcoinshistory.Add(tbl_Coinshistory);
                context.SaveChanges();
            }
            if (totalbadgedata.Where(x => x.BadgeId == (int)Custom.BadegIds.Apathfinder).FirstOrDefault() != null)
            {
                int marksgained = context.Tbl_Curriculamexamquestions.Where(x => x.Curriculamid == toalcurriculumdata.Where(y => y.IsCompleted == true).LastOrDefault().Curriculamid && x.IsCorrect == true).Count();
                if (marksgained == 10)
                {
                    Tbl_Badges data = new Tbl_Badges();
                    data.BadgeId = (int)Custom.BadegIds.Apathfinder;
                    data.SubUserId = Subuserid;
                    data.CreatedOn = DateTime.Now;
                    Insert(data);
                    Tbl_Curruculumavailablecoins coinsdata = context.Tbl_Curruculumavailablecoins.Where(x => x.SubUserId == Subuserid).FirstOrDefault();
                    coinsdata.TotalDiamonds = coinsdata.TotalDiamonds + deff_Badges.Where(x => x.BadgeId == (int)Custom.BadegIds.Apathfinder).FirstOrDefault().Diamondstobeawarded;
                    context.SaveChanges();
                    Tbl_Curruculumcoinshistory tbl_Coinshistory = new Tbl_Curruculumcoinshistory();
                    tbl_Coinshistory.Credit = deff_Badges.Where(x => x.BadgeId == (int)Custom.BadegIds.Apathfinder).FirstOrDefault().Diamondstobeawarded.ToString();
                    tbl_Coinshistory.Debit = "0";
                    tbl_Coinshistory.SubuserId = Subuserid;
                    tbl_Coinshistory.CreatedOn = DateTime.Now;
                    tbl_Coinshistory.balance = coinsdata.TotalDiamonds;
                    tbl_Coinshistory.TotalCoins = "";
                    tbl_Coinshistory.Messages = "1 diamond credited for Apathfinder";
                    tbl_Coinshistory.IsCoin = false;
                    context.Tbl_Curruculumcoinshistory.Add(tbl_Coinshistory);
                    context.SaveChanges();
                }
                //Tbl_Coinshistory tbl_Coinshistory = new Tbl_Coinshistory();
                //tbl_Coinshistory.Credit = deff_Badges.Where(x => x.BadgeId == (int)Custom.BadegIds.trigger).FirstOrDefault().Diamondstobeawarded.ToString();
                //tbl_Coinshistory.Debit = "0";
                //tbl_Coinshistory.SubuserId = Subuserid;
                //tbl_Coinshistory.CreatedOn = DateTime.Now;
                //tbl_Coinshistory.balance = coinsdata.TotalDiamonds;
            }
            if (totalbadgedata.Where(x => x.BadgeId == (int)Custom.BadegIds.MasterBlaster).FirstOrDefault() != null)
            {
                int totalcorrectanswers = context.Tbl_Curriculamexamquestions.Where(x => x.SubUserId == Subuserid && x.IsCorrect == true).Count();
                if (totalcorrectanswers >= 100)
                {
                    Tbl_Badges data = new Tbl_Badges();
                    data.BadgeId = (int)Custom.BadegIds.MasterBlaster;
                    data.SubUserId = Subuserid;
                    data.CreatedOn = DateTime.Now;
                    Insert(data);
                    Tbl_Curruculumavailablecoins coinsdata = context.Tbl_Curruculumavailablecoins.Where(x => x.SubUserId == Subuserid).FirstOrDefault();
                    coinsdata.TotalDiamonds = coinsdata.TotalDiamonds + deff_Badges.Where(x => x.BadgeId == (int)Custom.BadegIds.MasterBlaster).FirstOrDefault().Diamondstobeawarded;
                    context.SaveChanges();
                    Tbl_Curruculumcoinshistory tbl_Coinshistory = new Tbl_Curruculumcoinshistory();
                    tbl_Coinshistory.Credit = deff_Badges.Where(x => x.BadgeId == (int)Custom.BadegIds.MasterBlaster).FirstOrDefault().Diamondstobeawarded.ToString();
                    tbl_Coinshistory.Debit = "0";
                    tbl_Coinshistory.SubuserId = Subuserid;
                    tbl_Coinshistory.CreatedOn = DateTime.Now;
                    tbl_Coinshistory.balance = coinsdata.TotalDiamonds;
                    tbl_Coinshistory.TotalCoins = "";
                    tbl_Coinshistory.Messages = "1 diamond credited for MasterBlaster"; 
                    tbl_Coinshistory.IsCoin = false;
                    context.Tbl_Curruculumcoinshistory.Add(tbl_Coinshistory);
                    context.SaveChanges();
                }
                //Tbl_Coinshistory tbl_Coinshistory = new Tbl_Coinshistory();
                //tbl_Coinshistory.Credit = deff_Badges.Where(x => x.BadgeId == (int)Custom.BadegIds.trigger).FirstOrDefault().Diamondstobeawarded.ToString();
                //tbl_Coinshistory.Debit = "0";
                //tbl_Coinshistory.SubuserId = Subuserid;
                //tbl_Coinshistory.CreatedOn = DateTime.Now;
                //tbl_Coinshistory.balance = coinsdata.TotalDiamonds;
            }
            if (totalbadgedata.Where(x => x.BadgeId == (int)Custom.BadegIds.Logicgurustar).FirstOrDefault() != null)
            {
                int totalcorrectanswers = context.Tbl_Curriculamexamquestions.Where(x => x.SubUserId == Subuserid && x.IsCorrect == true).Count();
                if (totalcorrectanswers >= 200)
                {
                    Tbl_Badges data = new Tbl_Badges();
                    data.BadgeId = (int)Custom.BadegIds.Logicgurustar;
                    data.SubUserId = Subuserid;
                    data.CreatedOn = DateTime.Now;
                    Insert(data);
                    Tbl_Curruculumavailablecoins coinsdata = context.Tbl_Curruculumavailablecoins.Where(x => x.SubUserId == Subuserid).FirstOrDefault();
                    coinsdata.TotalDiamonds = coinsdata.TotalDiamonds + deff_Badges.Where(x => x.BadgeId == (int)Custom.BadegIds.Logicgurustar).FirstOrDefault().Diamondstobeawarded;
                    context.SaveChanges();
                    Tbl_Curruculumcoinshistory tbl_Coinshistory = new Tbl_Curruculumcoinshistory();
                    tbl_Coinshistory.Credit = deff_Badges.Where(x => x.BadgeId == (int)Custom.BadegIds.Logicgurustar).FirstOrDefault().Diamondstobeawarded.ToString();
                    tbl_Coinshistory.Debit = "0";
                    tbl_Coinshistory.SubuserId = Subuserid;
                    tbl_Coinshistory.CreatedOn = DateTime.Now;
                    tbl_Coinshistory.balance = coinsdata.TotalDiamonds;
                    tbl_Coinshistory.TotalCoins = "";
                    tbl_Coinshistory.Messages = "1 diamond credited for Logicgurustar";
                    tbl_Coinshistory.IsCoin = false;
                    context.Tbl_Curruculumcoinshistory.Add(tbl_Coinshistory);
                    context.SaveChanges();
                }
                //Tbl_Coinshistory tbl_Coinshistory = new Tbl_Coinshistory();
                //tbl_Coinshistory.Credit = deff_Badges.Where(x => x.BadgeId == (int)Custom.BadegIds.trigger).FirstOrDefault().Diamondstobeawarded.ToString();
                //tbl_Coinshistory.Debit = "0";
                //tbl_Coinshistory.SubuserId = Subuserid;
                //tbl_Coinshistory.CreatedOn = DateTime.Now;
                //tbl_Coinshistory.balance = coinsdata.TotalDiamonds;
            }
            if (totalbadgedata.Where(x => x.BadgeId == (int)Custom.BadegIds.Logicguruelite).FirstOrDefault() != null)
            {
                int totalcorrectanswers = context.Tbl_Curriculamexamquestions.Where(x => x.SubUserId == Subuserid && x.IsCorrect == true).Count();
                if (totalcorrectanswers >= 300)
                {
                    Tbl_Badges data = new Tbl_Badges();
                    data.BadgeId = (int)Custom.BadegIds.Logicguruelite;
                    data.SubUserId = Subuserid;
                    data.CreatedOn = DateTime.Now;
                    Insert(data);
                    Tbl_Curruculumavailablecoins coinsdata = context.Tbl_Curruculumavailablecoins.Where(x => x.SubUserId == Subuserid).FirstOrDefault();
                    coinsdata.TotalDiamonds = coinsdata.TotalDiamonds + deff_Badges.Where(x => x.BadgeId == (int)Custom.BadegIds.Logicguruelite).FirstOrDefault().Diamondstobeawarded;
                    context.SaveChanges();
                    Tbl_Curruculumcoinshistory tbl_Coinshistory = new Tbl_Curruculumcoinshistory();
                    tbl_Coinshistory.Credit = deff_Badges.Where(x => x.BadgeId == (int)Custom.BadegIds.Logicguruelite).FirstOrDefault().Diamondstobeawarded.ToString();
                    tbl_Coinshistory.Debit = "0";
                    tbl_Coinshistory.SubuserId = Subuserid;
                    tbl_Coinshistory.CreatedOn = DateTime.Now;
                    tbl_Coinshistory.balance = coinsdata.TotalDiamonds;
                    tbl_Coinshistory.TotalCoins = "";
                    tbl_Coinshistory.Messages = "1 diamond credited for Logicguruelite";
                    tbl_Coinshistory.IsCoin = false;
                    context.Tbl_Curruculumcoinshistory.Add(tbl_Coinshistory);
                    context.SaveChanges();
                }
                //Tbl_Coinshistory tbl_Coinshistory = new Tbl_Coinshistory();
                //tbl_Coinshistory.Credit = deff_Badges.Where(x => x.BadgeId == (int)Custom.BadegIds.trigger).FirstOrDefault().Diamondstobeawarded.ToString();
                //tbl_Coinshistory.Debit = "0";
                //tbl_Coinshistory.SubuserId = Subuserid;
                //tbl_Coinshistory.CreatedOn = DateTime.Now;
                //tbl_Coinshistory.balance = coinsdata.TotalDiamonds;
            }
            if (totalbadgedata.Where(x => x.BadgeId == (int)Custom.BadegIds.Oncloudnine).FirstOrDefault() != null)
            {
                int totaldrillsdone = toalcurriculumdata.Where(x => x.SubUserId == Subuserid && x.IsCompleted == true).Count();
                if (totaldrillsdone == 15)
                {
                    Tbl_Badges data = new Tbl_Badges();
                    data.BadgeId = (int)Custom.BadegIds.Oncloudnine;
                    data.SubUserId = Subuserid;
                    data.CreatedOn = DateTime.Now;
                    Insert(data);
                    Tbl_Curruculumavailablecoins coinsdata = context.Tbl_Curruculumavailablecoins.Where(x => x.SubUserId == Subuserid).FirstOrDefault();
                    coinsdata.TotalDiamonds = coinsdata.TotalDiamonds + deff_Badges.Where(x => x.BadgeId == (int)Custom.BadegIds.Oncloudnine).FirstOrDefault().Diamondstobeawarded;
                    context.SaveChanges();
                    Tbl_Curruculumcoinshistory tbl_Coinshistory = new Tbl_Curruculumcoinshistory();
                    tbl_Coinshistory.Credit = deff_Badges.Where(x => x.BadgeId == (int)Custom.BadegIds.Oncloudnine).FirstOrDefault().Diamondstobeawarded.ToString();
                    tbl_Coinshistory.Debit = "0";
                    tbl_Coinshistory.SubuserId = Subuserid;
                    tbl_Coinshistory.CreatedOn = DateTime.Now;
                    tbl_Coinshistory.balance = coinsdata.TotalDiamonds;
                    tbl_Coinshistory.TotalCoins = "";
                    tbl_Coinshistory.Messages = "1 diamond credited for Oncloudnine";
                    tbl_Coinshistory.IsCoin = false;
                    context.Tbl_Curruculumcoinshistory.Add(tbl_Coinshistory);
                    context.SaveChanges();
                }
                //Tbl_Coinshistory tbl_Coinshistory = new Tbl_Coinshistory();
                //tbl_Coinshistory.Credit = deff_Badges.Where(x => x.BadgeId == (int)Custom.BadegIds.trigger).FirstOrDefault().Diamondstobeawarded.ToString();
                //tbl_Coinshistory.Debit = "0";
                //tbl_Coinshistory.SubuserId = Subuserid;
                //tbl_Coinshistory.CreatedOn = DateTime.Now;
                //tbl_Coinshistory.balance = coinsdata.TotalDiamonds;
            }
            if (totalbadgedata.Where(x => x.BadgeId == (int)Custom.BadegIds.Marathon).FirstOrDefault() != null)
            {
                int totaldrillsdone = toalcurriculumdata.Where(x => x.SubUserId == Subuserid && x.IsCompleted == true).Count();
                if (totaldrillsdone == 30)
                {
                    Tbl_Badges data = new Tbl_Badges();
                    data.BadgeId = (int)Custom.BadegIds.Marathon;
                    data.SubUserId = Subuserid;
                    data.CreatedOn = DateTime.Now;
                    Insert(data);
                    Tbl_Curruculumavailablecoins coinsdata = context.Tbl_Curruculumavailablecoins.Where(x => x.SubUserId == Subuserid).FirstOrDefault();
                    coinsdata.TotalDiamonds = coinsdata.TotalDiamonds + deff_Badges.Where(x => x.BadgeId == (int)Custom.BadegIds.Marathon).FirstOrDefault().Diamondstobeawarded;
                    context.SaveChanges();
                    Tbl_Curruculumcoinshistory tbl_Coinshistory = new Tbl_Curruculumcoinshistory();
                    tbl_Coinshistory.Credit = deff_Badges.Where(x => x.BadgeId == (int)Custom.BadegIds.Marathon).FirstOrDefault().Diamondstobeawarded.ToString();
                    tbl_Coinshistory.Debit = "0";
                    tbl_Coinshistory.SubuserId = Subuserid;
                    tbl_Coinshistory.CreatedOn = DateTime.Now;
                    tbl_Coinshistory.balance = coinsdata.TotalDiamonds;
                    tbl_Coinshistory.TotalCoins = "";
                    tbl_Coinshistory.Messages = "1 diamond credited for Marathon";
                    tbl_Coinshistory.IsCoin = false;
                    context.Tbl_Curruculumcoinshistory.Add(tbl_Coinshistory);
                    context.SaveChanges();
                }
                //Tbl_Coinshistory tbl_Coinshistory = new Tbl_Coinshistory();
                //tbl_Coinshistory.Credit = deff_Badges.Where(x => x.BadgeId == (int)Custom.BadegIds.trigger).FirstOrDefault().Diamondstobeawarded.ToString();
                //tbl_Coinshistory.Debit = "0";
                //tbl_Coinshistory.SubuserId = Subuserid;
                //tbl_Coinshistory.CreatedOn = DateTime.Now;
                //tbl_Coinshistory.balance = coinsdata.TotalDiamonds;
            }
            //if (totalbadgedata.Where(x => x.BadgeId == (int)Custom.BadegIds.Marathon).FirstOrDefault() != null)
            //{
            //    int totaldrillsdone = toalcurriculumdata.Where(x => x.SubUserId == Subuserid && x.IsCompleted == true).Count();
            //    if (totaldrillsdone == 30)
            //    {
            //        Tbl_Badges data = new Tbl_Badges();
            //        data.BadgeId = (int)Custom.BadegIds.Marathon;
            //        data.SubUserId = Subuserid;
            //        data.CreatedOn = DateTime.Now;
            //        Insert(data);
            //        Tbl_Curruculumavailablecoins coinsdata = context.Tbl_Curruculumavailablecoins.Where(x => x.SubUserId == Subuserid).FirstOrDefault();
            //        coinsdata.TotalDiamonds = coinsdata.TotalDiamonds + deff_Badges.Where(x => x.BadgeId == (int)Custom.BadegIds.Marathon).FirstOrDefault().Diamondstobeawarded;
            //        context.SaveChanges();

            //    }
            //    //Tbl_Coinshistory tbl_Coinshistory = new Tbl_Coinshistory();
            //    //tbl_Coinshistory.Credit = deff_Badges.Where(x => x.BadgeId == (int)Custom.BadegIds.trigger).FirstOrDefault().Diamondstobeawarded.ToString();
            //    //tbl_Coinshistory.Debit = "0";
            //    //tbl_Coinshistory.SubuserId = Subuserid;
            //    //tbl_Coinshistory.CreatedOn = DateTime.Now;
            //    //tbl_Coinshistory.balance = coinsdata.TotalDiamonds;
            //}
            //At least 90% accuracy in any 1 topic
            if (totalbadgedata.Where(x => x.BadgeId == (int)Custom.BadegIds.Risingstar).FirstOrDefault() != null)
            {
                int marksgained = context.Tbl_Curriculamexamquestions.Where(x => x.Curriculamid == toalcurriculumdata.Where(y => y.IsCompleted == true).LastOrDefault().Curriculamid && x.IsCorrect == true).Count();

                if (marksgained == 9)
                {
                    Tbl_Badges data = new Tbl_Badges();
                    data.BadgeId = (int)Custom.BadegIds.Risingstar;
                    data.SubUserId = Subuserid;
                    data.CreatedOn = DateTime.Now;
                    Insert(data);
                    Tbl_Curruculumavailablecoins coinsdata = context.Tbl_Curruculumavailablecoins.Where(x => x.SubUserId == Subuserid).FirstOrDefault();
                    coinsdata.TotalDiamonds = coinsdata.TotalDiamonds + deff_Badges.Where(x => x.BadgeId == (int)Custom.BadegIds.Risingstar).FirstOrDefault().Diamondstobeawarded;
                    context.SaveChanges();
                    Tbl_Curruculumcoinshistory tbl_Coinshistory = new Tbl_Curruculumcoinshistory();
                    tbl_Coinshistory.Credit = deff_Badges.Where(x => x.BadgeId == (int)Custom.BadegIds.Risingstar).FirstOrDefault().Diamondstobeawarded.ToString();
                    tbl_Coinshistory.Debit = "0";
                    tbl_Coinshistory.SubuserId = Subuserid;
                    tbl_Coinshistory.CreatedOn = DateTime.Now;
                    tbl_Coinshistory.balance = coinsdata.TotalDiamonds;
                    tbl_Coinshistory.TotalCoins = "";
                    tbl_Coinshistory.Messages = "1 diamond credited for Risingstar";
                    tbl_Coinshistory.IsCoin = false;
                    context.Tbl_Curruculumcoinshistory.Add(tbl_Coinshistory);
                    context.SaveChanges();
                }
                //Tbl_Coinshistory tbl_Coinshistory = new Tbl_Coinshistory();
                //tbl_Coinshistory.Credit = deff_Badges.Where(x => x.BadgeId == (int)Custom.BadegIds.trigger).FirstOrDefault().Diamondstobeawarded.ToString();
                //tbl_Coinshistory.Debit = "0";
                //tbl_Coinshistory.SubuserId = Subuserid;
                //tbl_Coinshistory.CreatedOn = DateTime.Now;
                //tbl_Coinshistory.balance = coinsdata.TotalDiamonds;
            }
            //3 consecutive 100% scores in drills
            if (totalbadgedata.Where(x => x.BadgeId == (int)Custom.BadegIds.Tictactoe).FirstOrDefault() != null)
            {
                int marksgained = context.Tbl_Curriculamexamquestions.Where(x => x.Curriculamid == toalcurriculumdata.Where(y => y.IsCompleted == true).LastOrDefault().Curriculamid && x.IsCorrect == true).Count();

                if (marksgained == 9)
                {
                    Tbl_Badges data = new Tbl_Badges();
                    data.BadgeId = (int)Custom.BadegIds.Tictactoe;
                    data.SubUserId = Subuserid;
                    data.CreatedOn = DateTime.Now;
                    Insert(data);
                    Tbl_Curruculumavailablecoins coinsdata = context.Tbl_Curruculumavailablecoins.Where(x => x.SubUserId == Subuserid).FirstOrDefault();
                    coinsdata.TotalDiamonds = coinsdata.TotalDiamonds + deff_Badges.Where(x => x.BadgeId == (int)Custom.BadegIds.Tictactoe).FirstOrDefault().Diamondstobeawarded;
                    context.SaveChanges();
                    Tbl_Curruculumcoinshistory tbl_Coinshistory = new Tbl_Curruculumcoinshistory();
                    tbl_Coinshistory.Credit = deff_Badges.Where(x => x.BadgeId == (int)Custom.BadegIds.Tictactoe).FirstOrDefault().Diamondstobeawarded.ToString();
                    tbl_Coinshistory.Debit = "0";
                    tbl_Coinshistory.SubuserId = Subuserid;
                    tbl_Coinshistory.CreatedOn = DateTime.Now;
                    tbl_Coinshistory.balance = coinsdata.TotalDiamonds;
                    tbl_Coinshistory.TotalCoins = "";
                    tbl_Coinshistory.Messages = "1 diamond credited for Tictactoe";
                    tbl_Coinshistory.IsCoin = false;
                    context.Tbl_Curruculumcoinshistory.Add(tbl_Coinshistory);
                    context.SaveChanges();
                }
                //Tbl_Coinshistory tbl_Coinshistory = new Tbl_Coinshistory();
                //tbl_Coinshistory.Credit = deff_Badges.Where(x => x.BadgeId == (int)Custom.BadegIds.trigger).FirstOrDefault().Diamondstobeawarded.ToString();
                //tbl_Coinshistory.Debit = "0";
                //tbl_Coinshistory.SubuserId = Subuserid;
                //tbl_Coinshistory.CreatedOn = DateTime.Now;
                //tbl_Coinshistory.balance = coinsdata.TotalDiamonds;
            }
        }
    }
}
