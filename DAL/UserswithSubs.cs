﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
   public class UserswithSubs
    {

        public long UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Mobile { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public long SubUserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int ClassId { get; set; }
        public string ChildImage { get; set; }
        public string City { get; set; }
        public string School { get; set; }
        public Nullable<int> Marksgainedinpretest { get; set; }
        public Nullable<int> Marksgainedincurriculumpretest { get; set; }


    }
}
