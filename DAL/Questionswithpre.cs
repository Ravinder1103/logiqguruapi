﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class Questionswithpre:TBl_Questions
    {
        public long PreTestID { get; set; }
        public long SubUserId { get; set; }
        public long QuestionId { get; set; }
        public Nullable<bool> IsCorrect { get; set; }
        public Nullable<bool> IsAnswered { get; set; }
        public string Answered { get; set; }

    }
}
