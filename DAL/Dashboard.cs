﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
   public class Dashboard
    {
        public int Totalworksheets { get; set; }

        public int Pendingworksheets { get; set; }

        public decimal Completedworksheets { get; set; }

        public int Totalprofiency { get; set; }

        public List<Tbl_Worksheets> Worksheets { get; set; }

        public string Totalcoins { get; set; }

        public string Totaldiamonds { get; set; }

    }
}
