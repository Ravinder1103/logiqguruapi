﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class Badges
    {
        public long Id { get; set; }
        public long BadgeId { get; set; }
        public string BadgeName { get; set; }
        public string BadgeDescription { get; set; }
        public int Diamondstobeawarded { get; set; }
        public string Image { get; set; }

        public long SubUserId { get; set; }
        public System.DateTime CreatedOn { get; set; }

    }
}
