﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class Curirriculumwithlinks : Tbl_Curriculamworksheets
    {
        public string Videoname { get; set; }
        public string Pdfname { get; set; }

        public string Videolink { get; set; }
        public string Pdflink { get; set; }
    }
}
