﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class Questionscurriculum
    {
        public long CQId { get; set; }
        public long Q_Id { get; set; }
        public Nullable<bool> IsAnswered { get; set; }
        public Nullable<bool> IsCorrect { get; set; }
        public string Answergivenbyuser { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<System.DateTime> AnsweredOn { get; set; } 
        public Nullable<bool> IsCloneenabled { get; set; }
        public Nullable<long> CloneQ_Id { get; set; }
        public long WorksheetId { get; set; }
        public long SubUserId { get; set; }
        public int ChapterId { get; set; }
        public Nullable<int> QLevelID { get; set; }
        public Nullable<int> QTypeId { get; set; }
        public Nullable<int> SKillSetId { get; set; }
        public Nullable<int> PackageReferencesId { get; set; }
        public Nullable<int> SubTopicId { get; set; }
        public Nullable<int> ModuleId { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public string Sloution { get; set; }
        public string OptionA { get; set; }
        public string OPtionB { get; set; }
        public string OptionC { get; set; }
        public string OptionD { get; set; }
        public string OptionE { get; set; }
        public Nullable<int> Qstatus { get; set; }
        public Nullable<bool> Isactive { get; set; }
        public string TagIds { get; set; }
        public string Objective { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<long> UpdatedBy { get; set; }
        public Nullable<int> BoardId { get; set; }
        public Nullable<int> ClassId { get; set; }
        public Nullable<int> SubjectId { get; set; }
        public long Curriculamid { get; set; }

        //test purpose
        public string SubTopicName { get; set; }
        public string Text { get; set; }

        public  decimal TotalValue { get; set; }

        public Nullable<int> Attemptcount { get; set; }
        public string Wronganswergivenbyuser { get; set; }

    }
}
