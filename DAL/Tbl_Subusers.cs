//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tbl_Subusers
    {
        public long SubUserId { get; set; }
        public long UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int ClassId { get; set; }
        public string ChildImage { get; set; }
        public string City { get; set; }
        public string School { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<int> Marksgainedinpretest { get; set; }
        public Nullable<int> Marksgainedincurriculumpretest { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
    }
}
