//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tbl_Curriculamworksheets
    {
        public long Curriculamid { get; set; }
        public long SubUserId { get; set; }
        public long ChapterId { get; set; }
        public bool IsVideoopened { get; set; }
        public int Examtype { get; set; }
        public Nullable<bool> Isopened { get; set; }
        public Nullable<System.DateTime> Openeddate { get; set; }
        public Nullable<System.DateTime> Accessgranteddate { get; set; }
        public Nullable<bool> Tobeopened { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<bool> IsCompleted { get; set; }
        public Nullable<System.DateTime> Completeddate { get; set; }
    }
}
