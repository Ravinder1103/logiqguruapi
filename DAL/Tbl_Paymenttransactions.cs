//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tbl_Paymenttransactions
    {
        public long Id { get; set; }
        public string PaymenttransactionId { get; set; }
        public int No_of_Worksheetstobeassigned { get; set; }
        public bool Isworksheetsassigned { get; set; }
        public System.DateTime Createdon { get; set; }
        public int Status { get; set; }
        public string Message { get; set; }
        public Nullable<long> SubUserId { get; set; }
        public Nullable<long> SubjectId { get; set; }
        public Nullable<int> Amount { get; set; }
    }
}
