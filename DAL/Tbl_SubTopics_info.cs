//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tbl_SubTopics_info
    {
        public int SubTopicID { get; set; }
        public int ChapterId { get; set; }
        public string SubTopicName { get; set; }
        public string Discription { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public bool Status { get; set; }
        public Nullable<int> BoardId { get; set; }
        public Nullable<int> ClassId { get; set; }
        public Nullable<int> SubjectId { get; set; }
    }
}
