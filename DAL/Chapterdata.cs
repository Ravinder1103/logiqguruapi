﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
   public class Chapterdata
    {
        public int ChapterId { get; set; }
        public string Chaptername { get; set; }
        public Nullable<bool> IsAssessment { get; set; }
    }
}
