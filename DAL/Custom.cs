﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class Custom
    {
        public enum StatusCodeMessages
        {
            Unknown = 0,
            [Description("Data found.")]
            Datafound = 200,

            [Description("No Data Found.")]
            NoDataFound = 400,

            [Description("Record Already Exists.")]
            RecordAlreadyExists = 422,

            [Description("Record Not Modified.")]
            RecordNotModified = 304,

            [Description("Error Occured while Processing Request.")]
            ErrorOccuredwhileProcessingRequest = 500
        }
        public enum Status
        {
            Pending = 1,
            Approved = 2,
            Rejcted = 3
        }
        public enum PaymentStatus
        {
            Pending = 1,
            Success = 2,
            Failed = 3
        }
        public enum Subjectnames
        {
            PreTest = 1,
            Freeworksheet = 2,
        }
        public enum classname
        {
            Preprimary=1,
            Class1=2,
            Class2 =3,
            Class3 =4,
            Class4=5,
            Class5 =6,
            Class6=7,
            Class7=8,
            Class8=9,
            Class9=10,
            Class10= 11
        }
        public enum Primarychapternames
        {
            Identification=1,
            Observation=2,
            Memory=3,
            Concentration=4
        }
        public enum pretest
        {
            total=15,
        }
        public enum emtprimary
        {
            easych1=3,//first 3 easy chap 1
            mediumch1=5,//3 to 5 medium
            toughch1=6,//5to 6 tough
            easych2 =8,//6 to 8 easy chap2
            mediumch2 = 10,//8 to 10 medium
            toughch2 = 11,//10-11 tough
            easych3 = 13,//11-13 easy chap3
            mediumch3 = 14,//13-14 medium
            toughch3 = 15//14-15 tough

        }
        public enum emtaboveprimary
        {
            easych1 = 2,
            mediumch1 = 3,
            toughch1 = 4,
            easych2 = 6,
            mediumch2 = 8,
            toughch2 = 9,
            easych3 = 10,
            mediumch3 = 11,
            toughch3 = 12,
            easych4 = 13,
            mediumch4 = 14,
            toughch4 = 15

        }
        public enum Worksheetstatus
        {
            Created=1,
            Opened=2,
            Completed=3
        }
        public enum BadegIds
        {
            Upandloaded=1,
            trigger=3,
            Apathfinder=4,
            Risingstar=7,
            Tictactoe=8,
            HotStreak=9,
            Superstreak=10,
            MasterBlaster=11,
            Logicgurustar=12,
            Logicguruelite=13,
            Oncloudnine=14,
            Marathon=15,
            LogicChamp= 10006,
            Balancedlearner= 10007,
            LogicThinker=10017,
            onthetopfloor= 10019,
            Pictureperfect= 10020
        }
    }
}
