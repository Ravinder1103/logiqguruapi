﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class Worksheethistory
    {
        public long WorksheetId { get; set; }

        public DateTime? Completeddate { get; set; }

        public string Chaptername { get; set; }

        public int Marksgainedinthissheet { get; set; }

        public int Accuracy { get; set; }

        public int Coins { get; set; }

    }
}
