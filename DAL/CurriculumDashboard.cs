﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class CurriculumDashboard
    {
        public int Totalworksheets { get; set; }

        public int Pendingworksheets { get; set; }

        public decimal Completedworksheets { get; set; }

        public Nullable<decimal> Totalprofiency { get; set; }

        public List<Tbl_Curriculamworksheets> Worksheets { get; set; }

        public string Totalcoins { get; set; }

        public string Totaldiamonds { get; set; }

        public Nullable<int> Totalbadgesgained { get; set; }


    }
}
