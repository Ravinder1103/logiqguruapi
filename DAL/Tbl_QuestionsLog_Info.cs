//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tbl_QuestionsLog_Info
    {
        public long QlogId { get; set; }
        public long Q_Id { get; set; }
        public int Createdby { get; set; }
        public string Object { get; set; }
        public string CreatedOn { get; set; }
        public int QActionId { get; set; }
    }
}
