﻿using BAL;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VMD.RESTApiResponseWrapper.Net.Wrappers;

namespace LogiqGuruWebApi.Controllers
{
    public class WorksheetController : ApiController
    {
        public Worksheetrepository wrep = new Worksheetrepository();
        public Packagerepository prep = new Packagerepository();
        public Paymentrepoitory parep = new Paymentrepoitory();

        
        [Route("api/Worksheet/Createfreeworksheet")]
        public APIResponse Createfreeworksheet(long SubUserId, long Classid)
        {
            //Worksheetrepository rep = new Worksheetrepository();
            bool data = wrep.Insertfreeworksheets(SubUserId, Classid);
            if (data != false)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), data);
            else if (data == false)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.NotFound), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.NotFound), null);
            else
                throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));

        }
        //[Route("api/Worksheet/Createquetsionsforworksheet")]
        //public APIResponse Createfreeworksheet(long WorksheetId,long ClassId,long SubUserId)
        //{
        //    Worksheetrepository rep = new Worksheetrepository();
        //    bool data = rep.Insertworksheetquestions(WorksheetId,ClassId,SubUserId);
        //    if (data != false)
        //        return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), data);
        //    else if (data == false)
        //        return new APIResponse(Convert.ToInt16(HttpStatusCode.NotFound), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.NotFound), null);
        //    else
        //        throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));

        //}
        [HttpGet]
        [Route("api/Worksheet/Getworksheets")]
        public APIResponse Getworksheets(long SubUserId,int chapterid)
        {
            Worksheetrepository rep = new Worksheetrepository();
            List<Tbl_Worksheets> data = rep.Getworksheet(SubUserId,chapterid);
            if (data != null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), data);
            else if (data == null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.NotFound), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.NotFound), null);
            else
                throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));

        }
        [HttpGet]
        [Route("api/Worksheet/Getworksheetquestions")]
        public APIResponse Getworksheetquestions(long WorksheetId, long ClassId, long SubUserId,int chapterid)
        {
            //Worksheetrepository rep = new Worksheetrepository();
            List<Questionswithworksheet> data = null;
             data = wrep.Getquestionsofworksheet(WorksheetId);
            if (data.Count() == 0)
            {
                wrep.Insertworksheetquestions(WorksheetId, ClassId, SubUserId,chapterid);
                 data = wrep.Getquestionsofworksheet(WorksheetId);

            }
            if (data != null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), data);
            else if (data == null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.NotFound), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.NotFound), null);
            else
                throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));

        }
        [Route("api/Worksheet/Updateworksheetquestion")]

        public APIResponse Updateworksheetquestion(long Wqid, bool Answer, string selectedanswer,int totalquestions)
        {
            //Worksheetrepository rep = new Worksheetrepository();
            bool status = wrep.Updateworksheetwhenanswered(Wqid, Answer, selectedanswer, totalquestions);
            if (status)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), status);
            else
                throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));

        }
        [Route("api/Worksheet/Submitworksheet")]
        public APIResponse Submitworksheet(long Worksheetid,long SubUserId,int chapterid)
        {
            //Worksheetrepository rep = new Worksheetrepository();
            List<Questionswithworksheet> data = wrep.Submitworksheet(Worksheetid,SubUserId,chapterid);
            if (data != null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), data);
            else if (data == null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.NotFound), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.NotFound), null);
            else
                throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));

        }
        [HttpGet]
        [Route("api/Worksheet/Getcloneifexist")]
        public APIResponse Getcloneifexist(long Q_Id, long SubUserId,long WQId,long Duplicateqid)
        {
            //Worksheetrepository rep = new Worksheetrepository();
            Questionswithworksheet data = wrep.Fetchclonequestion(Q_Id, SubUserId, WQId, Duplicateqid);
            if (data != null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), data);
            else if (data == null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.NotFound), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.NotFound), null);
            else
                throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));

        }
        [HttpGet]
        [Route("api/Worksheet/GetWorksheetchapters")]
        public APIResponse GetWorksheetchapters( long SubUserId)
        {
            //Worksheetrepository rep = new Worksheetrepository();
            List<Chapterdata> data = wrep.Getchaptersofthisuser(SubUserId);
            if (data != null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), data);
            else if (data == null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.NotFound), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.NotFound), null);
            else
                throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));

        }
        [HttpGet]
        [Route("api/Worksheet/GetPackages")]
        public APIResponse GetPackages(long classid)
        {
            //Packagerepository rep = new Packagerepository();
            List<Tbl_Userpackages> data = prep.Getpackages(classid);
            if (data != null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), data);
            else if (data == null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.NotFound), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.NotFound), null);
            else
                throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));

        }
        [HttpPost]
        [Route("api/Worksheet/Makepayment")]
        public APIResponse Makepayment(decimal Amount,int Noofsheets,long SubUserId,int SubjectId)
        {
            //Paymentrepoitory rep = new Paymentrepoitory();
           long status= parep.Pendingpayment(Amount, Noofsheets,SubUserId,SubjectId);
            if (status!=0)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), status);
            else
                throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));

        }
        [HttpPost]
        [Route("api/Worksheet/Successpayment")]
        public APIResponse Successpayment(long Id,long ClassId,long SubUserId)
        {
            //Paymentrepoitory rep = new Paymentrepoitory();
            int status = parep.Successpayment(Id,ClassId, SubUserId);
            if (status!=0)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), status);
            else
                throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));
        }
        
        [Route("api/Worksheet/Checkpackageexist")]
        public APIResponse Checkpackageexist(long SubUserId)
        {
            //Paymentrepoitory rep = new Paymentrepoitory();
            bool status = parep.Checkpackageexist(SubUserId);
            if (status)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), status);
            else
                throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));
        }
        
             [Route("api/Worksheet/Checkpackageexistworksheet")]
        public APIResponse Checkpackageexistworksheet(long SubUserId)
        {
            //Paymentrepoitory rep = new Paymentrepoitory();
            bool status = parep.Checkpackageexistworksheet(SubUserId);
            if (status)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), status);
            else
                throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));
        }
        [HttpGet]
        [Route("api/Worksheet/Getworksheethistory")]
        public APIResponse Getworksheethistory(long SubUserId)
        {
            //Packagerepository rep = new Packagerepository();
            List<Worksheethistory> data = wrep.Getworksheethistory(SubUserId);
            if (data != null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), data);
            else if (data == null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.NotFound), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.NotFound), null);
            else
                throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));

        }


    }
}
