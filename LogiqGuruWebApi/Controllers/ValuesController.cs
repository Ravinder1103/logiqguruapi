﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VMD.RESTApiResponseWrapper.Net.Wrappers;

namespace LogiqGuruWebApi.Controllers
{

    public class ValuesController : ApiController
    {
        // GET api/values
        [HttpGet]
        [Route("api/IndividualMarketSubmit")]
        public APIResponse IndividualMarketSubmit([FromBody]string Hi)
        {
            bool status = false;
            if (Hi == "hi") {
                status = true;
            }
            if (status)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages),HttpStatusCode.OK), Hi);
             else
                throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));
        }
    }
}
