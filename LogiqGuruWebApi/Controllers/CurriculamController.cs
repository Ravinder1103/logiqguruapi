﻿using BAL;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VMD.RESTApiResponseWrapper.Net.Wrappers;

namespace LogiqGuruWebApi.Controllers
{
    public class CurriculamController : ApiController
    {
        public CurriculammainRepository crep = new CurriculammainRepository();
        [HttpGet]
        [Route("api/Curriculam/Curriculamchapters")]
        public APIResponse Curriculamchapters(long SubUserid)
        {
            List<Chapterdata> data = crep.Getcurriculamchapters(SubUserid);
            if (data != null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), data);
            else if (data == null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.NotFound), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.NotFound), null);
            else
                throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));

        }
        [HttpGet]
        [Route("api/Curriculam/Curriculumdrills")]
        public APIResponse Curriculumdrills(long ChapterId, long SubUserid)
        {
            List<Curirriculumwithlinks> data = crep.Getcurriculamdrills(ChapterId, SubUserid);
            if (data != null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), data);
            else if (data == null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.NotFound), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.NotFound), null);
            else
                throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));

        }
        [HttpGet]
        [Route("api/Curriculam/Curriculumassesmentdrills")]
        public APIResponse Curriculumassesmentdrills(long ChapterId, long SubUserid)
        {
            List<Curirriculumwithlinks> data = crep.Curriculumassesmentdrills(ChapterId, SubUserid);
            if (data != null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), data);
            else if (data == null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.NotFound), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.NotFound), null);
            else
                throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));

        }
        

        [HttpGet]
        [Route("api/Curriculam/Curriculumdrillsonload")]
        public APIResponse Curriculumdrillsonload(long SubUserid)
        {
            List<Curirriculumwithlinks> data = crep.Getcurriculamdrillsload(SubUserid);
            if (data != null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), data);
            else if (data == null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.NotFound), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.NotFound), null);
            else
                throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));

        }

        [HttpGet]
        [Route("api/Curriculam/Getcurriculumquestions")]
        public APIResponse Getcurriculumquestions(long Curriculamid, int Examtype, long Subuserid)
        {
            List<Questionscurriculum> data = null;
            data = crep.Getquestionsofworksheet(Curriculamid);
            if (data.Count() == 0)
            {
                crep.Insertworksheetquestions(Curriculamid, Examtype, Subuserid);
                data = crep.Getquestionsofworksheet(Curriculamid);

            }
            if (data != null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), data);
            else if (data == null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.NotFound), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.NotFound), null);
            else
                throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));

        }
        [HttpGet]
        [Route("api/Curriculam/Getcurriculumquestionsonload")]
        public APIResponse Getcurriculumquestionsonload(long Subuserid)
        {
            List<Questionscurriculum> data = null;
            Tbl_Curriculamworksheets CURDATA = crep.Getcurriculumlastid(Subuserid);
            if (CURDATA != null)
            {
                data = crep.Getquestionsofworksheet(CURDATA.Curriculamid);
                if (data.Count() == 0)
                {
                    crep.Insertworksheetquestions(CURDATA.Curriculamid, CURDATA.Examtype, Subuserid);
                    data = crep.Getquestionsofworksheet(CURDATA.Curriculamid);

                }
            }
            if (data != null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), data);
            else if (data == null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.NotFound), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.NotFound), null);
            else
                throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));

        }
        [Route("api/Curriculam/UpdateCurriculamquestion")]

        public APIResponse UpdateCurriculamquestion(long Cqid, bool Answer, string selectedanswer, int totalquestions)
        {
            //Worksheetrepository rep = new Worksheetrepository();
            bool status = crep.Updatecurriculumwhenanswered(Cqid, Answer, selectedanswer, totalquestions);
            if (status)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), status);
            else
                throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));

        }
        [Route("api/Curriculam/Updatewronganswercurriculum")]

        public APIResponse Updatewronganswercurriculum(long Cqid, bool Answer, string selectedanswer, int totalquestions)
        {
            //Worksheetrepository rep = new Worksheetrepository();
            bool status = crep.Updatewronganswercurriculum(Cqid, Answer, selectedanswer, totalquestions);
            if (status)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), status);
            else
                throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));

        }
        [Route("api/Curriculum/Submitcurriculum")]
        public APIResponse Submitcurriculum(long Cqid, long SubUserId, int chapterid)
        {
            Worksheetrepository wrep = new Worksheetrepository();
            List<Questionscurriculum> data = crep.Submitcurriculum(Cqid, SubUserId, chapterid);
            if (data != null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), data);
            else if (data == null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.NotFound), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.NotFound), null);
            else
                throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));

        }
        [HttpGet]
        [Route("api/Curriculum/GetPackages")]
        public APIResponse GetPackages(long classid)
        {
            Packagerepository rep = new Packagerepository();
            List<Tbl_Userpackages> data = rep.Getcpackages(classid);
            if (data != null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), data);
            else if (data == null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.NotFound), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.NotFound), null);
            else
                throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));

        }
        [HttpGet]
        [Route("api/Curriculam/Getworksheethistory")]
        public APIResponse Getworksheethistory(long SubUserId)
        {
            //Packagerepository rep = new Packagerepository();
            List<Worksheethistory> data = crep.Getcurriculumworksheethistory(SubUserId);
            if (data != null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), data);
            else if (data == null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.NotFound), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.NotFound), null);
            else
                throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));

        }
        [HttpGet]
        [Route("api/Curriculam/Getcurruculumbadge")]
        public APIResponse Getcurruculumbadge(long SubUserId)
        {
            Badgesrepository rep = new Badgesrepository();
            List<Badges> data = rep.Getcurruculumbadge(SubUserId);
            if (data != null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), data);
            else if (data == null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.NotFound), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.NotFound), null);
            else
                throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));

        }
        [HttpGet]
        [Route("api/Curriculam/Getcurriculummenuonload")]
        public APIResponse Getcurriculummenuonload(long Subuserid)
        {
            List<Questionscurriculum> data = null;
            Tbl_Curriculamworksheets CURDATA = crep.Getcurriculumlastid(Subuserid);
           
            if (CURDATA != null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), CURDATA);
            else if (data == null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.NotFound), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.NotFound), null);
            else
                throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));

        }


    }
}
