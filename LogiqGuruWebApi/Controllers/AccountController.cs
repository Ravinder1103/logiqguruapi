﻿using BAL;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VMD.RESTApiResponseWrapper.Net.Wrappers;

namespace LogiqGuruWebApi.Controllers
{
    public class AccountController : ApiController
    {
        [HttpPost]
        [Route("api/Registration")]
        public APIResponse Registration([FromBody]Tbl_Users users)
        {
            UserRepository rep = new UserRepository();
            bool status = rep.InsertUserdata(users);
            if (status)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), status);
            else
                throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));
        }
        [HttpGet]
        [Route("api/Account/Checkemail")]
        public APIResponse Checkemail([FromUri]string Email)
        {
            UserRepository rep = new UserRepository();
            bool status = rep.Checkemailexist(Email);
            if (status)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), status);
            else if(!status)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), status);

            else
                throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));
        }
        [HttpGet]
        [Route("api/Account/Checkmobile")]
        public APIResponse Checkmobile([FromUri]string Mobile)
        {
            UserRepository rep = new UserRepository();
            bool status = rep.Checkmobileexist(Mobile);
            if (status)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), status);
            else if (!status)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), status);
            else
                throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));
        }
        [HttpGet]
        [Route("api/Account/Login")]
        public APIResponse Login(string username,string password)
        {
            UserRepository rep = new UserRepository();
            Tbl_Users userdata = rep.Getuserdatabasedoncredentials(username, password);
            if (userdata!=null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), userdata);
            else if (userdata==null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.NotFound), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.NotFound), null);
            else
                throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));

        }
        [HttpGet]
        [Route("api/Account/Getclasses")]
        public APIResponse Getclasses()
        {
            Classrepository rep = new Classrepository();
            List<Tbl_Class_Info> classdata = rep.Getclasses();
            if (classdata != null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), classdata);
            else if (classdata == null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.NotFound), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.NotFound), null);
            else
                throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));

        }
        [HttpPost]
        [Route("api/Account/Addsubusers")]
        public APIResponse Addsubusers(Tbl_Subusers subusers)
        {
            Subusersrepository rep = new Subusersrepository();
            bool status = rep.Insersubusers(subusers);
            if (status)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), status);
            else
                throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));

        }
        [HttpPut]
        [Route("api/Account/Editsubusers")]
        public APIResponse Editsubusers(Tbl_Subusers subusers)
        {
            Subusersrepository rep = new Subusersrepository();
            bool status = rep.Updatesub(subusers);
            if (status)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), status);
            else
                throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));

        }
        [HttpGet]
        [Route("api/Account/Getmultiusers")]
        public APIResponse Getmultiusers(long Userid)
        {
            Subusersrepository rep = new Subusersrepository();
            List<Tbl_Subusers> subusers = rep.Get(x => x.UserId == Userid).ToList(); 
            if (subusers!=null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), subusers);
            else if (subusers == null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.NotFound), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.NotFound), null);
            else
                throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));

        }
        [HttpGet]
        [Route("api/Account/Gettoatluserdatabsedosubuserid")]
        public APIResponse Gettoatluserdatabsedosubuserid(long subuserid)
        {
            Subusersrepository rep = new Subusersrepository();
            UserswithSubs uws = rep.Getfulldataofuser(subuserid);
            //List<Tbl_Subusers> subusers = rep.Get(x => x.UserId == Userid).ToList();
            if (uws != null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), uws);
            else if (uws == null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.NotFound), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.NotFound), null);
            else
                throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));

        }
        [HttpPost]
        [Route("api/Account/Updateloggedinuserdata")]
        public APIResponse Updateloggedinuserdata(long Subuserid,long Userid)
        {
            Subusersrepository rep = new Subusersrepository();
            bool status = rep.updateisactiveforloggedin(Subuserid,Userid);
            if (status)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), status);
            else
                throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));

        }
        [HttpGet]
        [Route("api/Account/Getdashboarddata")]
        public APIResponse Getdashboarddata(long subuserid)
        {
            Worksheetrepository rep = new Worksheetrepository();
            Dashboard uws = rep.Getdashboarddata(subuserid);
            //List<Tbl_Subusers> subusers = rep.Get(x => x.UserId == Userid).ToList();
            if (uws != null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), uws);
            else if (uws == null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.NotFound), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.NotFound), null);
            else
                throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));

        }
        [HttpGet]
        [Route("api/Account/Getdashboarddatacurriculum")]
        public APIResponse Getdashboarddatacurriculum(long subuserid)
        {
            CurriculammainRepository rep = new CurriculammainRepository();
            CurriculumDashboard uws = rep.Getdashboarddata(subuserid);
            //List<Tbl_Subusers> subusers = rep.Get(x => x.UserId == Userid).ToList();
            if (uws != null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), uws);
            else if (uws == null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.NotFound), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.NotFound), null);
            else
                throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));

        }
        [HttpGet]
        [Route("api/Account/Getcoinshistory")]
        public APIResponse Getcoinshistory(long Subuserid)
        {
            Awardsrepository rep = new Awardsrepository();
            List<Tbl_Coinshistory> uws = rep.Getcoinshistory(Subuserid);
            //List<Tbl_Subusers> subusers = rep.Get(x => x.UserId == Userid).ToList();
            if (uws != null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), uws);
            else if (uws == null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.NotFound), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.NotFound), null);
            else
                throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));

        }
        [HttpGet]
        [Route("api/Account/Getcoinshistorycurriculum")]
        public APIResponse Getcoinshistorycurriculum(long Subuserid)
        {
            Awardscurriculumrepository rep = new Awardscurriculumrepository();
            List<Tbl_Curruculumcoinshistory> uws = rep.Getcoinshistory(Subuserid);
            //List<Tbl_Subusers> subusers = rep.Get(x => x.UserId == Userid).ToList();
            if (uws != null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), uws);
            else if (uws == null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.NotFound), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.NotFound), null);
            else
                throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));

        }
        [HttpGet]
        [Route("api/Account/Getdiamondshistorycurriculum")]
        public APIResponse Getdiamondshistorycurriculum(long Subuserid)
        {
            Awardscurriculumrepository rep = new Awardscurriculumrepository();
            List<Tbl_Curruculumcoinshistory> uws = rep.Getdiamondshistory(Subuserid);
            //List<Tbl_Subusers> subusers = rep.Get(x => x.UserId == Userid).ToList();
            if (uws != null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), uws);
            else if (uws == null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.NotFound), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.NotFound), null);
            else
                throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));

        }




    }
}
