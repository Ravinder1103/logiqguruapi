﻿using BAL;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VMD.RESTApiResponseWrapper.Net.Wrappers;

namespace LogiqGuruWebApi.Controllers
{
    public class VideoController : ApiController
    {
        public Videorepository vrep = new Videorepository();

        [Route("api/Video/Getvideos")]
        public APIResponse Getvideos(long ChapterId, long Classid)
        {
            //Worksheetrepository rep = new Worksheetrepository();
            List<Tbl_Content> data = vrep.Getvideos(ChapterId, Classid);
            if (data != null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), data);
            else if (data == null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.NotFound), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.NotFound), null);
            else
                throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));

        }
        [HttpGet]
        [Route("api/Video/Gettotalchapters")]
        public APIResponse Gettotalchapters(long ClassId)
        {
            List<Chapterdata> data = vrep.Getallchaptersforclass(ClassId);
            if (data != null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), data);
            else if (data == null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.NotFound), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.NotFound), null);
            else
                throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));
        }
    }
}
