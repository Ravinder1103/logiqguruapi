﻿using BAL;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VMD.RESTApiResponseWrapper.Net.Wrappers;

namespace LogiqGuruWebApi.Controllers
{
    public class PretestController : ApiController
    {
        public Pretestrepository prep = new Pretestrepository();
        public CurriculampretestRepository cprep = new CurriculampretestRepository();
        [HttpGet]
        [Route("api/Pretest/Getpretestquestions")]
        public APIResponse Getpretestquestions(long SubUserid)
        {
            QuestionsRepository rep = new QuestionsRepository();
            List<Questionswithpre> data = rep.Getquestions(SubUserid);
            if (data != null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), data);
            else if (data == null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.NotFound), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.NotFound), null);
            else
                throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));

        }
        [Route("api/Pretest/Updatepretest")]

        public APIResponse Updatepretest(long Pretestid, bool Answer, string selectedanswer,int totalquestions)
        {
            //Pretestrepository rep = new Pretestrepository();
            bool status = prep.Updatepresetwhenanswered(Pretestid,Answer, selectedanswer, totalquestions);
            if (status)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), status);
            else
                throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));

        }
        [Route("api/Pretest/Submitpretest")]
        public APIResponse Submitpretest(long Subuserid)
        {
            //Pretestrepository rep = new Pretestrepository();
            List<Questionswithpre> data = prep.Submitpretest(Subuserid);
            if (data != null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), data);
            else if (data == null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.NotFound), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.NotFound), null);
            else
                throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));

        }
        [HttpGet]
        [Route("api/Pretest/Getcurriculampretestquestions")]
        public APIResponse Getcurriculampretestquestions(long SubUserid)
        {
            CurriculamQuestionsRepository rep = new CurriculamQuestionsRepository();
            List<Questionswithpre> data = rep.Getquestions(SubUserid);
            if (data != null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), data);
            else if (data == null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.NotFound), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.NotFound), null);
            else
                throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));

        }
        [Route("api/Pretest/Updatecurriculampretest")]

        public APIResponse Updatecurriculampretest(long Pretestid, bool Answer, string selectedanswer, int totalquestions)
        {
            //Pretestrepository rep = new Pretestrepository();
            bool status = cprep.Updatepresetwhenanswered(Pretestid, Answer, selectedanswer, totalquestions);
            if (status)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), status);
            else
                throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));

        }
        [Route("api/Pretest/Submitcurriculampretest")]
        public APIResponse Submitcurriculampretest(long Subuserid)
        {
            //Pretestrepository rep = new Pretestrepository();
            List<Questionswithpre> data = cprep.Submitpretest(Subuserid);
            if (data != null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.OK), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.OK), data);
            else if (data == null)
                return new APIResponse(Convert.ToInt16(HttpStatusCode.NotFound), Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.NotFound), null);
            else
                throw new ApiException(Enum.GetName(typeof(Custom.StatusCodeMessages), HttpStatusCode.InternalServerError));

        }

    }
}
